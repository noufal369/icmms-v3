<?php

Route::get('/', function () {
    return redirect('admin');
});
Auth::routes();

Route::middleware(['auth'])->group(function () {
    # Change Company
    Route::get('/changeCompany/{id}', 'MasterController@changeCompany')->name('changeCompany');

    # Dashboard
    Route::get('', 'DashboardController@index')->name('data');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    # Complaints
    Route::get('/complaints/data', 'ComplaintController@data')->name('complaints.data');
    Route::resource('/complaints', 'ComplaintController');

    # Pm
    Route::get('/pm/data', 'PmController@data')->name('pm.data');
    Route::resource('/pm', 'PmController');

    # Checklist
    Route::get('/checklist/data', 'ChecklistController@data')->name('checklist.data');
    Route::resource('/checklist', 'ChecklistController');

    # Devices
    Route::get('/devices/data', 'DeviceController@data')->name('devices.data');
    Route::resource('/devices', 'DeviceController');

    # Suppliers
    Route::get('/suppliers/data', 'SupplierController@data')->name('suppliers.data');
    Route::resource('/suppliers', 'SupplierController');

    # ServiceProviders
    Route::get('/service_providers/data', 'ServiceProviderController@data')->name('service_providers.data');
    Route::resource('/service_providers', 'ServiceProviderController');

    # Manufacturers
    Route::get('/manufacturers/data', 'ManufacturerController@data')->name('manufacturers.data');
    Route::resource('/manufacturers', 'ManufacturerController');

    # Spare Parts
    Route::get('/spare_parts/data', 'SparePartController@data')->name('spare_parts.data');
    Route::resource('/spare_parts', 'SparePartController');

    # Accessories
    Route::get('/accessories/data', 'AccessoryController@data')->name('accessories.data');
    Route::resource('/accessories', 'AccessoryController');

    # Tutorials
    Route::get('/tutorials/data', 'TutorialController@data')->name('tutorials.data');
    Route::resource('/tutorials', 'TutorialController');

    # Departments
    Route::get('/departments/data', 'DepartmentController@data')->name('departments.data');
    Route::resource('/departments', 'DepartmentController');

    # Tutorials
    Route::get('/users/data', 'UserController@data')->name('users.data');
    Route::resource('/users', 'UserController');

    # Reports
    Route::get('/reports', 'ReportController@index')->name('report.index');

    # Demo
    Route::get('/{demopage?}', 'DemoController@demo')->name('demo');
    Route::get('/home', 'HomeController@index')->name('home');

    # Test Notifications
    Route::get('/notify', 'NotificationController@index')->name('notify');

    #Custom Route
    Route::any('/devices/{id}/create-pm', 'DeviceController@createPM')->name('devices.create-pm');

    Route::any('/complaints/{id}/log', 'ComplaintController@log')->name('complaints.log');

    Route::any('/pms/{id}/log', 'PmController@log')->name('pms.log');

    Route::any('/complaints/{id}/comment', 'ComplaintController@comment')->name('complaints.comment');

    Route::any('/complaints/{id}/progress', 'ComplaintController@progress')->name('complaints.progress');

    Route::any('/pms/{id}/progress', 'PmController@progress')->name('pms.progress');

});

