<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'Api\AuthController@login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '', 'middleware' => ['auth-api']], function () {
    Route::resource('complaints', 'Api\ComplaintController');
    Route::get('devices', 'Api\DeviceController@index');
    Route::get('errors', 'Api\DeviceController@errors');
});
