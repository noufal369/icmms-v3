<?php

namespace App\Services;

use App\Models\Company;
use App\Models\Department;
use Log;
use DB;
use Illuminate\Support\Facades\Artisan;

class DepartmentService extends BaseService
{
    /**
     * Get All Departments
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = Department::CompanyFetch()->with('company')->get();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single Department
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = Department::find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Create Department Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $device = Department::create($input);
            DB::commit();

            Artisan::call('icmms:update-metadata');

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $device->id;

    }


    /**
     * Update Department
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $fetch = Department::findOrfail($id);
            $fetch->update($input);
            DB::commit();

            Artisan::call('icmms:update-metadata');

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete Department
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete Department
            DB::beginTransaction();
            $item = Department::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }


    public function companies()
    {
        try {
            $query = Company::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    # End of Service
}
