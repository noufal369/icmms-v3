<?php

namespace App\Services;

use App\Models\Company;
use Log;

/**
 * Class MasterService
 * @package App\Services
 */
class MasterService
{
    /**
     * @param $id
     * @throws \Exception
     */
    public function changeCompany($id)
    {
        try {
            $company = Company::findOrFail($id);
            session(['company' => $company->name, 'company_id' => $company->id]);
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }




    # End of Service
}
