<?php


namespace App\Services;


use App\Models\Department;
use Illuminate\Support\Facades\DB;

class BaseService
{

    /**
     * Get Meta Data
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getMetaData()
    {
        $manufacturers = json_decode(DB::connection('sqlite')
            ->table('meta_data')
            ->where(['section' => 'manufacturers'])
            ->pluck('content')
            ->first());
        $devices = json_decode(DB::connection('sqlite')
            ->table('meta_data')
            ->where(['section' => 'devices'])
            ->pluck('content')
            ->first());
        $serviceProviders = json_decode(DB::connection('sqlite')
            ->table('meta_data')
            ->where(['section' => 'service_providers'])
            ->pluck('content')
            ->first());
        $departments = Department::CompanyFetch()->get();
        $suppliers = json_decode(DB::connection('sqlite')
            ->table('meta_data')
            ->where(['section' => 'suppliers'])
            ->pluck('content')
            ->first());
        $categories = json_decode(DB::connection('sqlite')
            ->table('meta_data')
            ->where(['section' => 'categories'])
            ->pluck('content')
            ->first());
        $errors = json_decode(DB::connection('sqlite')
            ->table('meta_data')
            ->where(['section' => 'errors'])
            ->pluck('content')
            ->first());
        try {
            $data = [
                'manufacturers' => $manufacturers,
                'serviceProviders' => $serviceProviders,
                'departments' => $departments,
                'suppliers' => $suppliers,
                'categories' => $categories,
                'errors' => $errors,
                'devices' => $devices
            ];
            return $data;
        } catch (\Exception $e) {
            \Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }
}