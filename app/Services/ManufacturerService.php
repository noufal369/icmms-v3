<?php

namespace App\Services;

use App\Models\Manufacturer;
use Log;
use DB;
use Illuminate\Support\Facades\Artisan;

class ManufacturerService
{
    /**
     * Get All Manufacturers
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = Manufacturer::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single Manufacturer
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = Manufacturer::find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Create Manufacturer Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $device = Manufacturer::create($input);
            DB::commit();

            Artisan::call('icmms:update-metadata');

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $device->id;

    }


    /**
     * Update Manufacturer
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $fetch = Manufacturer::findOrfail($id);
            $fetch->update($input);
            DB::commit();

            Artisan::call('icmms:update-metadata');

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete Manufacturer
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete Manufacturer
            DB::beginTransaction();
            $item = Manufacturer::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }


    # End of Service
}
