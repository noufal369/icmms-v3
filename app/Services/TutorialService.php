<?php

namespace App\Services;

use App\Models\Tutorial;
use Log;
use DB;

class TutorialService
{
    /**
     * Get All Tutorials
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = Tutorial::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single Tutorial
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = Tutorial::find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Create Tutorial Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $device = Tutorial::create($input);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $device->id;

    }


    /**
     * Update Tutorial
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $fetch = Tutorial::findOrfail($id);
            $fetch->update($input);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete Tutorial
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete Tutorial
            DB::beginTransaction();
            $item = Tutorial::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }


    # End of Service
}
