<?php

namespace App\Services;

use App\Models\Complaint;
use App\Models\Device;
use App\Models\Manufacturer;
use App\Models\ServiceProvider;
use App\Models\Supplier;
use App\Models\User;
use Log;

class DashboardService extends BaseService
{
    public function getData(): array
    {
        try {
            $data['device'] = Device::CompanyFetch()->count();
            $data['completed'] = Complaint::CompanyFetch()->where('progress', 'completed')->count();
            $data['ongoing'] = Complaint::CompanyFetch()->where('progress', 'ongoing')->count();
            $data['pending'] = Complaint::CompanyFetch()->where('progress', 'pending')->count();
            $data['suppliers'] = Supplier::count();
            $data['service_providers'] = ServiceProvider::count();
            $data['manufacturers'] = Manufacturer::count();
            $data['users'] = User::count();
            $data['manufacturers'] = Manufacturer::count();
            $data['complaints'] = 10;

            $devices = Device::CompanyFetch()->whereHas('complaints', function ($q) {
                $q->where('id', '>=', 1);
            })->take(100)->get();

            $data['salesCountTotal'] = 0;
            $data['purchaseCountTotal'] = 0;
            $data['chart'] = [];
            $data['chart1'] = [];
            $data['chart3'] = [];

            foreach ($devices as $key => $device) {
                if (isset($device->complaints)) {
                    $data['chart'][$key]['device'] = $device->name;
                    $data['chart'][$key]['complaints'] = isset($device->complaints) ? count($device->complaints) : 0;
                }
            }

            foreach ($devices as $key => $device) {
                if (isset($device->complaints)) {
                    $data['chart1'][$key]['device'] = $device->name;
                    $data['chart1'][$key]['complaints'] = isset($device->complaints) ? count($device->complaints) : 0;
                    $data['chart1'][$key]['amount'] = isset($device->complaints) ? $device->complaints->sum('cost') : 0;
                }
            }

            $colorArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
                '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
                '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
                '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
                '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
                '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];

            $data['chart3'] = Complaint::select(
                \DB::raw('YEAR(complained_at) as year'),
                \DB::raw('count(id) as count'))
                ->groupby('year')
                ->get()
                ->toArray();

            foreach ($data['chart3'] as $key => $char3) {
                $data['chart3'][$key]['color'] = $colorArray[array_rand($colorArray)];
            }


            $data['pm_devices'] = Device::CompanyFetch()->select('id', 'code', 'name', 'model_no', 'pm')
                //->where('pm', '<', now()->subMonth())
                ->where('pm', '<', date('Y-m-d', strtotime('+1 month')))
                ->get()
                ->toArray();
            $data['amc_devices'] = Device::CompanyFetch()->select('id', 'code', 'name', 'model_no', 'amc')
                ->where('amc', '<', now()->subMonth())
                ->get()
                ->toArray();
            $data['cmc_devices'] = Device::CompanyFetch()->select('id', 'code', 'name', 'model_no', 'cmc')
                ->where('cmc', '<', now()->subMonth())
                ->get()
                ->toArray();
            $data['warranty_devices'] = Device::CompanyFetch()->select('id', 'code', 'name', 'model_no', 'warranty')
                ->where('warranty', '<', now()->subMonth())
                ->get()
                ->toArray();
            $data['engineers'] = User::CompanyFetch()->select('id', 'name', 'email')
                ->where('user_type_id', '=', 2)
                ->get()
                ->toArray();

//            dd($data);

            return $data;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    # End of Service
}
