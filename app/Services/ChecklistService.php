<?php

namespace App\Services;

use App\Models\Checklist;
use App\Models\Department;
use App\Models\Device;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Auth;
use Log;
use App\Models\Error;

class ChecklistService extends BaseService
{
    /**
     * Get All Pms
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = Checklist::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single Pm
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = Checklist::Fetch()->find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    /**
     * Create Pm Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        #$input->validate([
        #   'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        #]);
//        $imageName = uniqid() . '.' . $input->image->getClientOriginalExtension();
//        $input->image->move(public_path('images'), $imageName);

//        $input['code'] = uniqid();
//        $input['status_id'] = 1;
//        $input['image'] = $imageName;

        # Data to Database Transaction
        try {

            $input['created_by'] = Auth::user()->id ?? 1;
            DB::beginTransaction();
            $device = Checklist::create($input);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $device->id;
    }


    /**
     * Update Pm
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $fetch = Checklist::findOrfail($id);
            $fetch->update($input);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete Pm
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete Pm
            DB::beginTransaction();
            $item = Checklist::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }


    public function users($id)
    {
        try {
            $query = User::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    # End of Service
}
