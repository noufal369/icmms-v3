<?php

namespace App\Services;

use App\Models\User;
use App\Models\Department;
use App\Models\UserType;
use App\Models\Company;
use Illuminate\Support\Facades\Hash;
use Log;
use DB;

class UserService
{
    /**
     * Get All Users
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = User::CompanyFetch();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single User
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = User::find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Create User Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        # Data to Database Transaction
        try {
            $input['password'] = Hash::make($input['password']);
            $input['api_token'] = (string) \Str::uuid();
            DB::beginTransaction();
            $device = User::create($input);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $device->id;

    }


    /**
     * Update User
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        # Data to Database Transaction
        try {
            $input['password'] = Hash::make($input['password']);
            DB::beginTransaction();
            $fetch = User::findOrfail($id);
            $fetch->update($input);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete User
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete User
            DB::beginTransaction();
            $item = User::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }

    public function department()
    {
        try {
            $query = Department::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function usertype()
    {
        try {
            $query = UserType::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function company()
    {
        try {
            $query = Company::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    # End of Service
}
