<?php

namespace App\Services;

use App\Models\Pm;
use App\Models\Department;
use App\Models\Device;
use App\Models\User;
use App\Models\SparePart;
use App\Models\Checklist;
use App\Models\UserLogPm;
use DB;
use Illuminate\Support\Facades\Auth;
use Log;
use App\Models\Error;

class PmService extends BaseService
{
    /**
     * Get All Pms
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = Pm::Fetch()->CompanyFetch();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single Pm
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = Pm::Fetch()->find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    /**
     * Create Pm Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        #$input->validate([
        #   'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        #]);

        if (array_key_exists('document', $input)) {
            $imageName = uniqid() . '.' . $input['document']->getClientOriginalExtension();
            $input['document']->move(public_path('uploads/images'), $imageName);
            $input['document'] = $imageName;
        }

        if (array_key_exists('spare_part_id', $input)) {
            $parts = $input['spare_part_id'];
            $input['spare_part_id'] = json_encode($parts);
        }

        if (array_key_exists('pm_checklist', $input)) {
            $checklist = $input['pm_checklist'];
            $input['pm_checklist'] = json_encode($checklist);
        }

        $input['progress'] = 'pending';
        $input['pm_at'] = now();
        $input['pm_by'] = Auth::user()->id ?? 1;
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $device = Pm::create($input);
            DB::commit();

            $order['order_no'] = 'PM_NO_'.$device->id;
            DB::beginTransaction();
            $fetch = Pm::where('id', $device->id);
            if(isset($fetch)) {
                $fetch->update($order);
            }
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $device->id;
    }


    /**
     * Update Pm
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        if (array_key_exists('document', $input)) {
            $imageName = uniqid() . '.' . $input['document']->getClientOriginalExtension();
            $input['document']->move(public_path('uploads/images'), $imageName);
            $input['document'] = $imageName;
        }

        if (array_key_exists('spare_part_id', $input)) {
            $parts = $input['spare_part_id'];
            $input['spare_part_id'] = json_encode($parts);
        }

        if(array_key_exists('pm_checklist', $input)){
            $checklist = $input['pm_checklist'];
            $input['pm_checklist'] = json_encode($checklist);
        }

        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $fetch = Pm::where('id', $id);
            if(isset($fetch)) {
                $fetch->update($input);
            } else {
                Pm::create($input);
            }
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete Pm
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete Pm
            DB::beginTransaction();
            $item = Pm::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }


    public function users()
    {
        try {
            $query = User::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function spares()
    {
        try {
            $query = SparePart::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function device($id)
    {
        try {
            $query = Device::Fetch()->find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function checklist()
    {
        try {
            $query = Checklist::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    public function getLog($id)
    {
        try {
            new UserLogPm();
            $query = UserLogPm::fetch()
                    ->where('pm_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->get()
                    ->toArray();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    public function log($id, $type, $device_id)
    {
        new UserLogPm();
        if($type == 1)
        {
            $input['user_id'] = Auth::user()->id ?? 1;
            $input['pm_id'] = $id;
            $input['device_id'] = $device_id;
            $input['created_at'] = now();
            $input['status'] = $type;
            $input['start_time'] = now();
            DB::beginTransaction();
            UserLogPm::create($input);
            DB::commit();
        }

        if($type == 2)
        {
            $query = UserLogPm::fetch()
                    ->where('pm_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->limit(1)
                    ->get()
                    ->toArray();

            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $query[0]['start_time']);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', now());

            $result = $to->diffInMinutes($from); 

            $old_log = $query[0]['log_time'];

            $input['log_time'] = $old_log + $result;

            $input['status'] = $type;
            DB::beginTransaction();
            $fetch = UserLogPm::findOrfail($query[0]['id']);
            $fetch->update($input);
            DB::commit();
        }

        if($type == 3)
        {
            $query = UserLogPm::fetch()
                    ->where('pm_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->limit(1)
                    ->get()
                    ->toArray();

            $input['start_time'] = now();
            $input['status'] = $type;
            DB::beginTransaction();
            $fetch = UserLogPm::findOrfail($query[0]['id']);
            $fetch->update($input);
            DB::commit();
        }

        if($type == 4)
        {
            $query = UserLogPm::fetch()
                    ->where('pm_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->limit(1)
                    ->get()
                    ->toArray();

            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $query[0]['start_time']);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', now());

            $result = $to->diffInMinutes($from); 
            $old_log = $query[0]['log_time'];

            $input['log_time'] = $old_log + $result;
            $input['status'] = $type;
            DB::beginTransaction();
            $fetch = UserLogPm::findOrfail($query[0]['id']);
            $fetch->update($input);
            DB::commit();
        }

        $query = UserLogPm::fetch()
                    ->where('pm_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->limit(1)
                    ->get()
                    ->toArray();

        return $query;
    }


    public function progress($id, $progress)
    {
        $input['progress'] = $progress;
            DB::beginTransaction();
            $fetch = Pm::findOrfail($id);
            $fetch->update($input);
            DB::commit();
    }

    # End of Service
}
