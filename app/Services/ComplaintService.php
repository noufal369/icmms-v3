<?php

namespace App\Services;

use App\Models\Complaint;
use App\Models\Department;
use App\Models\Device;
use App\Models\User;
use App\Models\ComplaintComments;
use App\Models\ComplaintUsers;
use App\Models\SparePart;
use App\Models\UserLogComplaint;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Log;
use App\Models\Error;

class ComplaintService extends BaseService
{
    /**
     * Get All Complaints
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = Complaint::Fetch()->CompanyFetch();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    /**
     * Get All Complaints
     * @return mixed
     * @throws \Exception
     */
    public function index()
    {
        try {
            $query = Complaint::Fetch()->paginate();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single Complaint
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = Complaint::Fetch()->find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    /**
     * Create Complaint Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        # Data to Database Transaction
        try {

            $device = Device::findOrfail($input['device_id']);

            $input['assigned_to'] = '["'.$device->preferred_engineer.'"]';

            $input['progress'] = 'pending';
            $input['code'] = uniqid();
            $input['status_id'] = 0;
            $input['complained_at'] = now();
            $input['complained_by'] = Auth::user()->id ?? 1;
            DB::beginTransaction();
            $device = Complaint::create($input);
            DB::commit();

            $fetch = Complaint::findOrfail($device->id);
            $order['order_no'] = 'WO_NO_'.$device->id;
            $fetch->update($order);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $device->id;
    }


    /**
     * Update Complaint
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        $complaint = Complaint::find($id);
        if(array_key_exists('image', $input)){
            $imageName = uniqid() . '.' . $input['image']->getClientOriginalExtension();
            $input['image']->move(public_path('uploads/images'), $imageName);
            $input['image'] = $imageName;
        }

        if(array_key_exists('spare_part_id', $input)){
            $spares = $input['spare_part_id'];
            $input['spare_part_id'] = json_encode($spares);
        }
        if(array_key_exists('assigned_to', $input)){
            $assigned_to = $input['assigned_to'];
            $input['assigned_to'] = json_encode($assigned_to);
        }

        if(!empty($assigned_to)){
            foreach($assigned_to as $item){
                new ComplaintUsers();
                $assigned['complaint_id'] = $id;
                $assigned['user_id'] = $item;
                $assigned['created_at'] = now();

                $user = User::find($item);

                if (isset($user)) {
                    $newData = [
                        'woNo' => $complaint->order_no,
                        'name' => $user->name,
                        'email' => $user->email,
                        'customer' => $complaint->device->department->name ?? '',
                        'link' => route('complaints.edit', $id),
                        'complaint' => $complaint->title ?? ''
                    ];
                    Mail::send('mail', $newData, function($message) use ($newData) {
                        $message->to($newData['email'], $newData['name'])->subject
                        ('You have got a new complaint assigned');
                        $message->from('alert.icmms@gmail.com','Alisha iCMMS');
                    });
                }

                DB::beginTransaction();
                ComplaintUsers::create($assigned);
                DB::commit();
            }
        }

        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $fetch = Complaint::findOrfail($id);
            $fetch->update($input);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete Complaint
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete Complaint
            DB::beginTransaction();
            $item = Complaint::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }


    public function users($id)
    {
        try {
            $query = User::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function comments($id)
    {
        try {
            $query = ComplaintComments::fetch()
                    ->where('complaint_id', '=', $id)
                    ->orderBy('created_at', 'ASC')
                    ->get()
                    ->toArray();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function spares()
    {
        try {
            $query = SparePart::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function getLog($id)
    {
        try {
            new UserLogComplaint();
            $query = UserLogComplaint::fetch()
                    ->where('complaint_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->get()
                    ->toArray();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function getFullLog($id)
    {
        try {
            new UserLogComplaint();
            $query = UserLogComplaint::fetch()
                    ->where('complaint_id', '=', $id)
                    ->get()
                    ->toArray();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function log($id, $type)
    {
        new UserLogComplaint();
        if($type == 1)
        {
            $input['user_id'] = Auth::user()->id ?? 1;
            $input['complaint_id'] = $id;
            $input['created_at'] = now();
            $input['status'] = $type;
            $input['start_time'] = now();
            DB::beginTransaction();
            UserLogComplaint::create($input);
            DB::commit();
        }

        if($type == 2)
        {
            $query = UserLogComplaint::fetch()
                    ->where('complaint_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->limit(1)
                    ->get()
                    ->toArray();

            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $query[0]['start_time']);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', now());

            $result = $to->diffInMinutes($from);

            $old_log = $query[0]['log_time'];

            $input['log_time'] = $old_log + $result;

            $input['status'] = $type;
            DB::beginTransaction();
            $fetch = UserLogComplaint::findOrfail($query[0]['id']);
            $fetch->update($input);
            DB::commit();
        }

        if($type == 3)
        {
            $query = UserLogComplaint::fetch()
                    ->where('complaint_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->limit(1)
                    ->get()
                    ->toArray();

            $input['start_time'] = now();
            $input['status'] = $type;
            DB::beginTransaction();
            $fetch = UserLogComplaint::findOrfail($query[0]['id']);
            $fetch->update($input);
            DB::commit();
        }

        if($type == 4)
        {
            $query = UserLogComplaint::fetch()
                    ->where('complaint_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->limit(1)
                    ->get()
                    ->toArray();

            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $query[0]['start_time']);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', now());

            $result = $to->diffInMinutes($from);
            $old_log = $query[0]['log_time'];

            $input['log_time'] = $old_log + $result;
            $input['status'] = $type;
            DB::beginTransaction();
            $fetch = UserLogComplaint::findOrfail($query[0]['id']);
            $fetch->update($input);
            DB::commit();
        }

        $query = UserLogComplaint::fetch()
                    ->where('complaint_id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->limit(1)
                    ->get()
                    ->toArray();

        return $query;
    }


    public function comment($id, $comment)
    {
        new ComplaintComments();

        $input['user_id'] = Auth::user()->id ?? 1;
        $input['complaint_id'] = $id;
        $input['created_at'] = now();
        $input['comment'] = $comment;

        DB::beginTransaction();
        ComplaintComments::create($input);
        DB::commit();

        $query = ComplaintComments::fetch()
                    ->where('complaint_id', '=', $id)
                    ->orderBy('created_at', 'ASC')
                    ->get()
                    ->toArray();

        return $query;

    }

    public function progress($id, $progress)
    {
        $input['progress'] = $progress;
            DB::beginTransaction();
            $fetch = Complaint::findOrfail($id);
            $fetch->update($input);
            DB::commit();
    }

    # End of Service
}
