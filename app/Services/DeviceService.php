<?php

namespace App\Services;

use App\Models\Department;
use App\Models\Device;
use App\Models\Checklist;
use App\Models\User;
use App\Models\SparePart;
use App\Models\Pm;
use App\Models\Error;
use LaravelQRCode\Facades\QRCode;
use Log;
use DB;
use Illuminate\Support\Facades\Artisan;

class DeviceService extends BaseService
{
    /**
     * Get All Devices
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = Device::FetchMin()->CompanyFetch()->orderBy('id', 'DESC');
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single Device
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = Device::Fetch()->find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    /**
     * Create Device Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        #$input->validate([
        #   'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        #]);
        $serial = $input['serial_no'];

        if(array_key_exists('checklist', $input)){
            $checklist = $input['checklist']; 
            $input['checklist'] = json_encode($checklist);
        }

        if(array_key_exists('spare_parts', $input)){
            $spares = $input['spare_parts']; 
            $input['spare_parts'] = json_encode($spares);
        }

        if(!empty($serial)){
            foreach($serial as $item){

                if(array_key_exists('image', $input)){
                    $imageName = uniqid() . '.' . $input['image']->getClientOriginalExtension();
                    $input['image']->move(public_path('uploads/images'), $imageName);
                    $input['image'] = $imageName;
                }        

                $name = $input['name'];
                $qr_image = uniqid();
                QRCode::text($name)->setOutfile(public_path('uploads/qr_codes/' . $qr_image . '.png'))->png();

                $input['photo'] = '';
                $input['code'] = uniqid();
                $input['status_id'] = 1;
                $input['qr_code'] = $qr_image . '.png';

                $input['serial_no'] = $item;
                        
                # Data to Database Transaction
                try {
                    DB::beginTransaction();
                    $device = Device::create($input);
                    DB::commit();

                    Artisan::call('icmms:update-metadata');

                } catch (\Exception $e) {
                    DB::rollback();
                    Log::info('Log message', array('error' => $e));
                    throw $e;
                }

            }
        }

        return $device->id;

    }


    /**
     * Update Device
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        if(array_key_exists('image', $input)){
            $imageName = uniqid() . '.' . $input['image']->getClientOriginalExtension();
            $input['image']->move(public_path('uploads/images'), $imageName);
            $input['image'] = $imageName;
        }        

        $name = $input['name'];
        $qr_image = uniqid();
        QRCode::text($name)->setOutfile(public_path('uploads/qr_codes/' . $qr_image . '.png'))->png();

        $input['qr_code'] = $qr_image . '.png';

        if(array_key_exists('checklist', $input)){
            $checklist = $input['checklist']; 
            $input['checklist'] = json_encode($checklist);
        }

        if(array_key_exists('spare_parts', $input)){
            $spares = $input['spare_parts']; 
            $input['spare_parts'] = json_encode($spares);
        }

        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $fetch = Device::findOrfail($id);
            $fetch->update($input);
            DB::commit();

            Artisan::call('icmms:update-metadata');

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete Device
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete Device
            DB::beginTransaction();
            $item = Device::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }


    public function checklist()
    {
        try {
            $query = Checklist::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function users()
    {
        try {
            $query = User::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function spares()
    {
        try {
            $query = SparePart::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function device($id)
    {
        try {
            $query = Device::Fetch()->find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function error_codes()
    {
        try {
            $query = Error::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }

    public function storePM($input)
    {
        #$input->validate([
        #   'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        #]);

        if (array_key_exists('document', $input)) {
            $imageName = uniqid() . '.' . $input['document']->getClientOriginalExtension();
            $input['document']->move(public_path('uploads/images'), $imageName);
            $input['document'] = $imageName;
        }

        if (array_key_exists('spare_part_id', $input)) {
            $parts = $input['spare_part_id'];
            $input['spare_part_id'] = json_encode($parts);
        }

        $input['progress'] = 'pending';
        $input['pm_at'] = now();
        $input['pm_by'] = Auth::user()->id ?? 1;
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $device = Pm::create($input);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $device->id;
    }

    public function errors()
    {
        try {
            $query = Error::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    # End of Service
}
