<?php

namespace App\Services;

use App\Models\SparePart;
use LaravelQRCode\Facades\QRCode;
use Log;
use DB;
use Illuminate\Support\Facades\Artisan;

class SparePartService
{
    /**
     * Get All SpareParts
     * @return mixed
     * @throws \Exception
     */
    public function getAll()
    {
        try {
            $query = SparePart::all();
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Get Single SparePart
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $query = SparePart::find($id);
            return $query;
        } catch (\Exception $e) {
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
    }


    /**
     * Create SparePart Service
     * @param $input
     * @return bool
     * @throws \Exception
     */
    public function store($input)
    {
        $serial = $input['serial_no'];
        $warranty = $input['warranty_date'];
        if(!empty($serial)){
            foreach($serial as $key => $item){
                if(array_key_exists('image', $input)){
                    $imageName = uniqid() . '.' . $input['image']->getClientOriginalExtension();
                    $input['image']->move(public_path('uploads/images'), $imageName);
                    $input['image'] = $imageName;
                }
                $name = $input['name'];
                $qr_image = uniqid();
                QRCode::text($name)->setOutfile(public_path('uploads/qr_codes/' . $qr_image . '.png'))->png();
                $input['qr_code'] = $qr_image . '.png';
                $input['serial_no'] = $item;
                $input['warranty_date'] = $warranty[$key];  

                try {
                    DB::beginTransaction();
                    $device = SparePart::create($input);
                    DB::commit();

                    Artisan::call('icmms:update-metadata');

                } catch (\Exception $e) {
                    DB::rollback();
                    Log::info('Log message', array('error' => $e));
                    throw $e;
                }
            }
        }
        # Data to Database Transaction
        
        return $device->id;

    }


    /**
     * Update SparePart
     * @param $input
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($input, $id)
    {
        if(array_key_exists('image', $input)){
            $imageName = uniqid() . '.' . $input['image']->getClientOriginalExtension();
            $input['image']->move(public_path('uploads/images'), $imageName);
            $input['image'] = $imageName;
        }
        $name = $input['name'];
        $qr_image = uniqid();
        QRCode::text($name)->setOutfile(public_path('uploads/qr_codes/' . $qr_image . '.png'))->png();
        $input['qr_code'] = $qr_image . '.png';
        
        # Data to Database Transaction
        try {
            DB::beginTransaction();
            $fetch = SparePart::findOrfail($id);
            $fetch->update($input);
            DB::commit();

            Artisan::call('icmms:update-metadata');

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;

    }

    /**
     * Delete SparePart
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            # Delete SparePart
            DB::beginTransaction();
            $item = SparePart::withTrashed()->findOrFail($id);

            if (!$item->trashed())
                $item->delete();
            else
                $item->restore();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::info('Log message', array('error' => $e));
            throw $e;
        }
        return $id;
    }


    # End of Service
}
