<?php

namespace App\Console\Commands;

use App\Models\Device;
use App\Models\Error;
use Illuminate\Console\Command;
use App\Models\Department;
use App\Models\Supplier;
use App\Models\Manufacturer;
use App\Models\Category;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\DB;

class MetaDataUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'icmms:update-metadata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Update of MetaData';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return bool
     */
    public function handle()
    {
        try {
            # Categories
            $data = [
                [
                    'section' => 'departments',
                    'content' => Department::all()
                ],
                [
                    'section' => 'suppliers',
                    'content' => Supplier::all()
                ],
                [
                    'section' => 'manufacturers',
                    'content' => Manufacturer::all()
                ],
                [
                    'section' => 'categories',
                    'content' => Category::all()
                ],
                [
                    'section' => 'service_providers',
                    'content' => ServiceProvider::all()
                ],
                [
                    'section' => 'devices',
                    'content' => Device::all()
                ],
                [
                    'section' => 'errors',
                    'content' => Error::all()
                ],
            ];

            $metaDataExisting = DB::connection('sqlite')->table('meta_data')->get();
            if (isset($metaDataExisting) and count($metaDataExisting) > 0) {
                DB::connection('sqlite')->table('meta_data')->delete();
            }
            DB::connection('sqlite')->table('meta_data')->insert($data);

            $this->info('Updated Meta Data Successfully');
        } catch (\Exception $e) {
            $this->error("Something went wrong, " . $e->getMessage());
        }

        return true;
    }
}
