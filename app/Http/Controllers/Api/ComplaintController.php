<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreComplaint;
use App\Models\Complaint;
use App\Services\ComplaintService;

class ComplaintController extends Controller
{
    public function __construct(ComplaintService $service)
    {
        $this->service = $service;
    }

    public function store(StoreComplaint $request)
    {
        try {
            $id = $this->service->store($request->all());
            $complaint = Complaint::find($id);

        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return response()->json(['success' => [
            'code' => $complaint->code,
            'id' => $complaint->id
        ]], 200);
    }

    public function index()
    {
        try {
            $complaints = $this->service->index();
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return response()->json(['success' => true, 'data' => $complaints], 200);
    }
}
