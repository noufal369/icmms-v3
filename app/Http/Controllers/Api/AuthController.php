<?php


namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthController
{
    public function login(Request $request)
    {
        try {
            $token = null;
            $status = false;
            $rules['email'] = 'required|email';
            $rules['password'] = 'required';

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            if (Auth::attempt($request->all())) {
                $token = Str::uuid();
                User::where('email', $request['email'])->update(['api_token' => $token]);
                $status = true;
            }
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return response()->json(['success' => $status, 'token' => $token], 200);
    }
}
