<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Models\Error;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        try {
            $devices = Device::select(['id', 'name'])
                ->where('name', 'like', '%' . ($request['search'] ?? '') . '%')
                ->orderBy('name')
                ->paginate();
        } catch (\Exception $e) {
            throw $e;
        }
        return response()->json(['success' => true, 'data' => $devices], 200);
    }

    public function errors(): JsonResponse
    {
        try {
            $errors = Error::get(['id', 'name']);
        } catch (\Exception $e) {
            throw $e;
        }
        return response()->json(['success' => true, 'data' => $errors], 200);
    }
}
