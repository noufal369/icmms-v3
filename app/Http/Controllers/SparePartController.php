<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSparePart;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use App\Services\SparePartService as Service;
use Illuminate\Http\Request;

/**
 * Class SparePartController
 * @package App\Http\Controllers
 */
class SparePartController extends Controller
{
    /**
     * @var Service $service
     */
    private $service;

    /**
     * SparePartController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get Data
     * @return mixed
     * @throws \Exception
     */
    public function data()
    {
        try {
            $get = $this->service->getAll();
            return Datatables::of($get)
                ->addColumn('actions', function ($data) {
                    $html = '<a href="javascript:void(0);" data-delete="' . $data->id . '" class="btn btn-danger btn-sm btn-icon btn-icon-mini btn-round">';
                    if (!is_null($data->deleted_at))
                        $html .= '<i class="fa fa-recycle"  aria-hidden="true"></i>';
                    else
                        $html .= '<i class="fa fa-trash-o"  aria-hidden="true"></i>';

                    $html .= '</a>  &nbsp; ';
                    $html .= '<a href="#" onclick="viewEdit(' . $data->id . ')" class="btn btn-primary btn-sm btn-icon btn-icon-mini btn-round">';
                    $html .= '<i class="fa fa-pencil-square-o" data-name="edit" title="Edit" aria-hidden="true"></i>';
                    $html .= '</a>  &nbsp; ';
                    $html .= '<a href="javascript:void(0);" class="btn btn-success btn-sm btn-icon btn-icon-mini btn-round">';
                    $html .= '<i data-toggle="modal" data-target=".show" class="fa fa-eye" data-name="view"  data-view="' . $data->id . '" title="View data"  aria-hidden="true"></i> ';
                    $html .= '</a> &nbsp; ';
                    return $html;
                })
                ->addColumn('updated_at', function ($get) {
                    return $get->updated_at->diffForHumans();
                })
                ->rawColumns(['actions'])->make(true);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Listing SparePart
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        try {
            return view('admin.spare_part.index');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Create SparePart
     * @throws \Throwable
     */
    public function create()
    {
        try {
            $html = "";
            $html .= view('admin.spare_part.create')->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function show($id)
    {
        try {
            $html = "";
            $data['get'] = $this->service->get($id);
            $html .= view('admin.spare_part.view', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Storing SparePart
     * @param StoreSparePart $spare_part
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $spare_part)
    {
        try {
            $this->service->store($spare_part->except("_token"));
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('spare_parts.index')->withSuccess('New SparePart Added !!');
    }


    /**
     * API for getting a Single SparePart
     * @param $id
     * @throws \Throwable
     */
    public function edit($id)
    {
        try {
            $html = "";
            $data['get'] = $this->service->get($id);
            $html .= view('admin.spare_part.edit', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * API for Updating a Single SparePart
     * @param StoreSparePart $spare_part
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(Request $spare_part, $id)
    {
        try {
            $this->service->update($spare_part->except("_token"), $id);
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('spare_parts.index')->withSuccess('New SparePart Added !!');
    }

    /**
     * API for Deleting a SpareParts
     * @param $id
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $this->service->destroy($id);
            $response = ['status' => 201, 'data' => 'deleted'];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

}

