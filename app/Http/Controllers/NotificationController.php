<?php

namespace App\Http\Controllers;

use App\Notifications\TestNotify;
use Illuminate\Http\Request;

/**
 * Class NotificationController
 * @package App\Http\Controllers
 */
class NotificationController extends Controller
{
    public function index()
    {
        $user = \App\User::find(2);
//
        $user->notify(new TestNotify());

        $query = $user->unreadNotifications();

        $notifications = $query->get()->each(function ($n) {
            $n->created = $n->created_at->toIso8601String();
        });

        $total = $user->unreadNotifications->count();

        pr($query->get()->toArray(), true);
//
//        $data = array("to" => "cNf2---6Vs9", "notification" => array( "title" => "Shareurcodes.com", "body" => "A Code Sharing Blog!","icon" => "icon.png", "click_action" => "http://shareurcodes.com"));
//        $data_string = json_encode($data);
//        echo "The Json Data : ".$data_string;
//        $headers = array ( 'Authorization: key=' . 'AIzaSyCjfZ2mBk7zMqRPntkRE4KrOjVWic7fZPg', 'Content-Type: application/json' );
//        $ch = curl_init(); curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
//        curl_setopt( $ch,CURLOPT_POST, true );
//        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
//        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
//        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);
//        $result = curl_exec($ch);
//        curl_close ($ch);
//        echo "<p>&nbsp;</p>";
//        echo "The Result : ".$result;
//        return view('admin.notify');
    }
}
