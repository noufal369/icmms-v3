<?php

namespace App\Http\Controllers;

use App\Services\MasterService as Service;
use Illuminate\Support\Facades\Redirect;

/**
 * Class ManufacturerController
 * @package App\Http\Controllers
 */
class MasterController extends Controller
{
    /**
     * @var Service $service
     */
    private $service;

    /**
     * ManufacturerController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function changeCompany($id)
    {
        try {
            $this->service->changeCompany($id);
            return back();
        } catch (\Exception $e) {
            throw $e;
        }
    }



}

