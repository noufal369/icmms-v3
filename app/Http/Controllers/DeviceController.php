<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDevice;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use App\Services\DeviceService as Service;
use Illuminate\Http\Request;
use DateTime;
/**
 * Class DeviceController
 * @package App\Http\Controllers
 */
class DeviceController extends Controller
{
    /**
     * @var Service $service
     */
    private $service;

    /**
     * DeviceController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get Data
     * @return mixed
     * @throws \Exception
     */
    public function data()
    {
        try {
            $get = $this->service->getAll();
            return Datatables::of($get)
                ->addColumn('actions', function ($data) {
                    $html = '<a href="javascript:void(0);" data-delete="' . $data->id . '" class="btn btn-danger btn-sm btn-icon btn-icon-mini btn-round">';
                    if (!is_null($data->deleted_at))
                        $html .= '<i class="fa fa-recycle"  aria-hidden="true"></i>';
                    else
                        $html .= '<i class="fa fa-trash-o"  aria-hidden="true"></i>';

                    $html .= '</a>  &nbsp; ';
                    $html .= '<a href="#" onclick="viewEdit(' . $data->id . ')" class="btn btn-primary btn-sm btn-icon btn-icon-mini btn-round">';
                    $html .= '<i class="fa fa-pencil-square-o" data-name="edit" title="Edit" aria-hidden="true"></i>';
                    $html .= '</a>  &nbsp; ';
                    $html .= '<a href="/devices/'.$data->id.'" class="btn btn-success btn-sm btn-icon btn-icon-mini btn-round">';
                    $html .= '<i data-toggle="modal" data-target=".show" class="fa fa-eye" data-name="view"  data-view="' . $data->id . '" title="View data"  aria-hidden="true"></i> ';
                    $html .= '</a> &nbsp; ';
                    return $html;
                })
                ->addColumn('code', function ($data) {
                    $html = '<a href="javascript:void(0);">';
                    $html .= $data->code;
                    $html .= '</a> &nbsp; ';
                    return $html;
                })
                // ->addColumn('updated_at', function($get) {
                //     return $get->updated_at->diffForHumans();
                // })
                ->addColumn('warranty', function($get) {
                    $html = ' <span class="badge badge-info">' . \Carbon\Carbon::parse($get->warranty)->toFormattedDateString() . '</span>';
                    return $html;
                })
                // ->addColumn('amc', function($get) {
                //     $html = ' <span class="badge badge-info">' . \Carbon\Carbon::parse($get->amc)->toFormattedDateString() . '</span>';
                //     return $html;
                // })
                // ->addColumn('cmc', function($get) {
                //     $html = ' <span class="badge badge-info">' . \Carbon\Carbon::parse($get->cmc)->toFormattedDateString() . '</span>';
                //     return $html;
                // })
                ->addColumn('pm', function($get) {
                    $html = ' <span class="badge badge-info">' . \Carbon\Carbon::parse($get->pm)->toFormattedDateString() . '</span>';
                    return $html;
                })

                ->rawColumns(['actions', 'code', 'purchased_at', 'warranty', 'amc', 'cmc', 'pm'])->make(true);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        try {
            return view('admin.device.index');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function create()
    {
        try {
            $html = "";
            $data = $this->service->getMetaData();
            $data['checklist'] = $this->service->checklist();
            $data['spare_parts'] = $this->service->spares();
            $data['users'] = $this->service->users();
            $html .= view('admin.device.create', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function show($id)
    {
        try {
            $html = "";
            $data = $this->service->getMetaData();
            $data['get'] = $this->service->get($id);
            $data['spare_parts'] = $this->service->spares($id);
            $data['users'] = $this->service->users();

            $userArray = [];
            if(!empty($data['users'])){
                foreach($data['users'] as $item){
                    $userArray[$item->id] = $item->name;
                }
            }
            $data['userArray'] = $userArray;
            $data['get']['age_of_device'] = '';
            if(!is_null($data['get']['installed_at'])) {

                $to = \Carbon\Carbon::createFromFormat('Y-m-d', $data['get']['installed_at']);
                $from = \Carbon\Carbon::createFromFormat('Y-m-d', date('Y-m-d'));

                $interval = $from->diff($to);
                $data['get']['age_of_device'] =  $interval->format('%y').' Years ' .$interval->format('%m').' Months  ' .$interval->format('%d').' Days';
            }
            

            return view('admin.device.view', $data);
            // $html .= view('admin.device.view', $data)->render();
            // $response = ['status' => 201, 'data' => $html];
            // echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Storing Device
     * @param StoreDevice $device
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $device)
    {
        try {
            $this->service->store($device->except("_token"));
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('devices.index')->withSuccess('New Device Added !!');
    }


    /**
     * API for getting a Single Device
     * @param $id
     * @throws \Throwable
     */
    public function edit($id)
    {
        try {
            $html = "";
            $data = $this->service->getMetaData();
            $data['get'] = $this->service->get($id);
            $data['checklist'] = $this->service->checklist();
            $data['spare_parts'] = $this->service->spares();
            $data['users'] = $this->service->users();
            $html .= view('admin.device.edit', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * API for Updating a Single Device
     * @param StoreDevice $device
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(Request $device, $id)
    {
        try {
            $this->service->update($device->except("_token"), $id);
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('devices.index')->withSuccess('New Device Added !!');
    }

    /**
     * API for Deleting a Devices
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $this->service->destroy($id);
            $response = ['status' => 201, 'data' => 'deleted'];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function createPM($id)
    {
        try {
            $html = "";
            $data = $this->service->getMetaData();
            $data['id'] = $id;
            $data['get'] = $this->service->get($id);
            $data['users'] = $this->service->users();
            $data['spares'] = $this->service->spares();
            $data['checklist'] = $this->service->checklist();
            $check = [];
            if(!empty($data['checklist'])){
                foreach($data['checklist'] as $val){
                    $check[$val->id] = $val->title;
                }
            }
            $data['check'] = $check;
            return view('admin.pm.create', $data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

}

