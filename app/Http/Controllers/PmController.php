<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePm;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use App\Services\PmService as Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * Class PmController
 * @package App\Http\Controllers
 */
class PmController extends Controller
{
    /**
     * @var Service $service
     */
    private $service;

    /**
     * PmController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get Data
     * @return mixed
     * @throws \Exception
     */
    public function data()
    {
        try {
            $data = $this->service->getAll();
            return Datatables::of($data)
                ->addColumn('actions', function ($data) {
                    $html = '<a href="' . route('pm.edit', $data->id) .'" class="btn btn-primary btn-sm btn-icon btn-icon-mini btn-round">';
                    $html .= '<i class="fa fa-pencil-square-o" data-name="edit" title="Edit" aria-hidden="true"></i>';
                    $html .= '</a> &nbsp; ';
                    // $html .= '<a href="#" onclick="viewEdit(' . $data->id . ')" class="btn btn-success btn-sm btn-icon btn-icon-mini btn-round">';
                    // $html .= '<i data-toggle="modal" data-target=".show" class="fa fa-eye" data-name="view"  data-view="' . $data->id . '" title="View data"  aria-hidden="true"></i> ';
                    // $html .= '</a> &nbsp; ';
                    return $html;
                })
                ->addColumn('wo_no', function ($data) {
                    return $data->order_no;
                })
                ->addColumn('pm', function ($data) {
                    $html = '<span class="badge badge-light">' . (isset($data->pm->name) ? $data->pm->name : "") . ' </span>';
                    $html .= ' <span class="badge badge-info">' . (isset($data->pm_at) ? \Carbon\Carbon::parse($data->pm_at)->toFormattedDateString() : "") . '</span>';
                    return $html;
                })
                ->addColumn('attend', function ($data) {
                    $html = '<span class="badge badge-light">' . (isset($data->pm->name) ? $data->pm->name : "") . ' </span>';
                    $html .= ' <span class="badge badge-info">' . (isset($data->responded_at) ? $data->responded_at->toFormattedDateString() : "") . '</span>';
                    return $html;
                })
                ->addColumn('close', function ($data) {
                    $html = '<span class="badge badge-light">' . (isset($data->close->name) ? $data->close->name : "") . ' </span>';
                    $html .= ' <span class="badge badge-info">' . (isset($data->closed_at) ? $data->closed_at->toFormattedDateString() : "") . '</span>';
                    return $html;
                })
                ->setRowClass(function ($data) {
                    return $data->device->category->name === "Critical" ? 'alert-danger' : '';
                })
                ->addColumn('attend', function ($data) {
                    $html = '<span class="badge badge-light">' . (isset($data->attend->name) ? $data->attend->name : "") . ' </span>';
                })
                ->addColumn('progress', function ($data) {
                    if ($data->progress === "completed")
                        $html = '<span class="badge badge-success">COMPLETED</span>';
                    elseif ($data->progress === "ongoing")
                        $html = '<span class="badge badge-warning">IN PROGRESS</span>';
                    else
                        $html = '<span class="badge badge-danger">PENDING</span>';

                    return $html;
                })
                ->rawColumns(['progress', 'actions', 'pm', 'attend', 'close'])->make(true);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        try {
            return view('admin.pm.index');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function show($id)
    {
        try {
            $html = "";
            $data['get'] = $this->service->get($id);
            $html .= view('admin.pm.view', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Storing Pm
     * @param StorePm $request
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $request)
    {
        try {
            $this->service->store($request->except("_token"));
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('dashboard')->withSuccess('New Pm Added !!');
    }


    /**
     * API for getting a Single Pm
     * @param $id
     * @throws \Throwable
     */
    public function edit($id)
    {
        try {
            $html = "";
            $data = $this->service->getMetaData();
            $data['id'] = $id;
            $data['get'] = $this->service->get($id);
            $data['device'] = $this->service->device($data['get']['device_id']);
            $data['users'] = $this->service->users();
            $data['spares'] = $this->service->spares();
            $data['checklist'] = $this->service->checklist();
            $data['log'] = $this->service->getLog($id);
            $check = [];
            if(!empty($data['checklist'])){
                foreach($data['checklist'] as $val){
                    $check[$val->id] = $val->title;
                }
            }
            $data['check'] = $check;
            $userArray = [];
            if(!empty($data['users'])){
                foreach($data['users'] as $item){
                    $userArray[$item->id] = $item->name;
                }
            }
            $data['userArray'] = $userArray;
            return view('admin.pm.edit', $data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * API for Updating a Single Pm
     * @param StorePm $request
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        try {
            $id = $this->service->update($request->except("_token", "_method"), $id);
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('pm.index')->withSuccess('New Pm Added !!');
    }

    /**
     * API for Deleting a Pms
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $this->service->destroy($id);
            $response = ['status' => 201, 'data' => 'deleted'];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    public function log($id)
    {
        $type = request()->get('type');
        $device_id = request()->get('device_id');
        $html = '';
        $get = $this->service->get($id);
        $users = $this->service->users($id);
        $userArray = [];
            if(!empty($users)){
                foreach($users as $item){
                    $userArray[$item->id] = $item->name;
                }
            }

        try {
            $log = $this->service->log($id, $type, $device_id);

            if(!empty($log)){
                if($log[0]['status'] != 1){ 
                $html .= '<div class="alert alert-info">
                    <strong>User : </strong>';

                    if(!is_null($get->assigned_to)) {                         
                        $html .= $userArray[$get->assigned_to];                       
                    }
                $html .= '<br>
                    <strong>Total Time Worked : </strong>';
                    
                    if($log[0]['log_time']  < 60){
                        $html .= $log[0]['log_time']. ' Minutes';
                    }
                    else{
                        $hours = (int)($log[0]['log_time']/60);
                        $minutes = $log[0]['log_time'] % 60;

                        $html .= $hours. ' Hour '.$minutes.' Minutes';
                    }
                $html .= '</div>';
                }
            }

            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function progress($id)
    {
        $progress = request()->get('progress');

        try {
                $this->service->progress($id, $progress);
                $response = ['status' => 201, 'data' => 'success'];
                echo json_encode($response);

        }catch (\Exception $e) {
            throw $e;
        }

    }

}

