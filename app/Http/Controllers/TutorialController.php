<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTutorial;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use App\Services\TutorialService as Service;
use Illuminate\Http\Request;

/**
 * Class TutorialController
 * @package App\Http\Controllers
 */
class TutorialController extends Controller
{
    /**
     * @var Service $service
     */
    private $service;

    /**
     * TutorialController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get Data
     * @return mixed
     * @throws \Exception
     */
    public function data()
    {
        try {
            $get = $this->service->getAll();
            return Datatables::of($get)
                ->addColumn('actions', function ($data) {
                    $html = '<a href="javascript:void(0);" data-delete="' . $data->id . '">';
                    if (!is_null($data->deleted_at))
                        $html .= '<i class="fa fa-recycle"  aria-hidden="true"></i>';
                    else
                        $html .= '<i class="fa fa-trash-o"  aria-hidden="true"></i>';

                    $html .= '</a>  &nbsp; ';
                    $html .= '<a href="#" onclick="viewEdit(' . $data->id . ')" >';
                    $html .= '<i class="fa fa-pencil-square-o" data-name="edit" title="Edit" aria-hidden="true"></i>';
                    $html .= '</a>  &nbsp; ';
                    $html .= '<a href="javascript:void(0);">';
                    $html .= '<i data-toggle="modal" data-target=".show" class="fa fa-eye" data-name="view"  data-view="' . $data->id . '" title="View data"  aria-hidden="true"></i> ';
                    $html .= '</a> &nbsp; ';
                    return $html;
                })
                ->addColumn('updated_at', function ($get) {
                    return $get->updated_at->diffForHumans();
                })
                ->rawColumns(['actions'])->make(true);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Listing Tutorial
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        try {
            return view('admin.tutorial.index');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Create Tutorial
     * @throws \Throwable
     */
    public function create()
    {
        try {
            $html = "";
            $html .= view('admin.tutorial.create')->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function show($id)
    {
        try {
            $html = "";
            $data['get'] = $this->service->get($id);
            $html .= view('admin.tutorial.view', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Storing Tutorial
     * @param StoreTutorial $tutorial
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $tutorial)
    {
        try {
            $this->service->store($tutorial->except("_token"));
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('tutorials.index')->withSuccess('New Tutorial Added !!');
    }


    /**
     * API for getting a Single Tutorial
     * @param $id
     * @throws \Throwable
     */
    public function edit($id)
    {
        try {
            $html = "";
            $data['get'] = $this->service->get($id);
            $html .= view('admin.tutorial.edit', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * API for Updating a Single Tutorial
     * @param StoreTutorial $tutorial
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(Request $tutorial, $id)
    {
        try {
            $this->service->update($tutorial->except("_token"), $id);
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('tutorials.index')->withSuccess('New Tutorial Added !!');
    }

    /**
     * API for Deleting a Tutorials
     * @param $id
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $this->service->destroy($id);
            $response = ['status' => 201, 'data' => 'deleted'];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

}

