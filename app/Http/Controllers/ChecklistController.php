<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChecklist;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use App\Services\ChecklistService as Service;
use Illuminate\Http\Request;

/**
 * Class PmController
 * @package App\Http\Controllers
 */
class ChecklistController extends Controller
{
    /**
     * @var Service $service
     */
    private $service;

    /**
     * PmController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get Data
     * @return mixed
     * @throws \Exception
     */
    public function data()
    {
        try {
            $data = $this->service->getAll();
            return Datatables::of($data)
                ->addColumn('actions', function ($data) {
                    $html = '<a href="#" onclick="viewEdit(' . $data->id . ')" class="btn btn-primary btn-sm btn-icon btn-icon-mini btn-round">';
                    $html .= '<i class="fa fa-pencil-square-o" data-name="edit" title="Edit" aria-hidden="true"></i>';
                    $html .= '</a> &nbsp; ';
                    
                    return $html;
                })
                ->addColumn('title', function ($data) {
                    $html = isset($data->title) ? $data->title:'';
                    return $html;
                })
                // ->addColumn('device', function ($data) {
                //     $html = isset($data->device->name) ? $data->device->name:'';
                    
                //     return $html;
                // })
               
                ->rawColumns(['title', 'actions', 'device'])->make(true);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        try {
            return view('admin.checklist.index');
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function create()
    {
        try {
            $html = "";
            $data = $this->service->getMetaData();
            $html .= view('admin.checklist.create', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function show($id)
    {
        try {
            $html = "";
            $data['get'] = $this->service->get($id);
            $html .= view('admin.checklist.view', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Storing Pm
     * @param StorePm $request
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $request)
    {
        try {
            $this->service->store($request->except("_token"));
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('checklist.index')->withSuccess('New Pm Added !!');
    }


    /**
     * API for getting a Single Pm
     * @param $id
     * @throws \Throwable
     */
    public function edit($id)
    {
        try {
            $html = "";
            $data = $this->service->getMetaData();
            $data['get'] = $this->service->get($id);
            $data['users'] = $this->service->users($id);
            $html .= view('admin.checklist.edit', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * API for Updating a Single Pm
     * @param StorePm $request
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        try {
            $this->service->update($request->except("_token"), $id);
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('checklist.index')->withSuccess('New Pm Added !!');
    }

    /**
     * API for Deleting a Pms
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $this->service->destroy($id);
            $response = ['status' => 201, 'data' => 'deleted'];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

}

