<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreComplaint;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use App\Services\ComplaintService as Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * Class ComplaintController
 * @package App\Http\Controllers
 */
class ComplaintController extends Controller
{
    /**
     * @var Service $service
     */
    private $service;

    /**
     * ComplaintController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get Data
     * @return mixed
     * @throws \Exception
     */
    public function data()
    {
        try {
            $data = $this->service->getAll();
            return Datatables::of($data)
                ->addColumn('actions', function ($data) {
                    $html = '<a href="' . route('complaints.edit', $data->id) .'" class="btn btn-primary btn-sm btn-icon btn-icon-mini btn-round">';
                    $html .= '<i class="fa fa-pencil-square-o" data-name="edit" title="Edit" aria-hidden="true"></i>';
                    $html .= '</a>';
                    // $html .= '<a href="#" onclick="viewEdit(' . $data->id . ')" >';
                    // $html .= '<i data-toggle="modal" data-target=".show" class="fa fa-eye" data-name="view"  data-view="' . $data->id . '" title="View data"  aria-hidden="true"></i> ';
                    // $html .= '</a> &nbsp; ';
                // if($data->progress == 'completed') {
                //     $html .= '<a href="javascript:void(0);" class="btn btn-success btn-sm btn-icon btn-icon-mini btn-round">';
                //     $html .= '<i data-toggle="modal" data-target=".show" class="fa fa-eye" data-name="view"  data-view="' . $data->id . '" title="View data"  aria-hidden="true"></i> ';
                //     $html .= '</a>';
                //     }
                    return $html;
                })
                ->addColumn('complaint', function ($data) {
                    $html = '<span class="badge badge-light">' . (isset($data->complaint->name) ? $data->complaint->name : "") . ' </span>';
                    $html .= ' <span class="badge badge-info">' . (isset($data->complained_at) ? $data->complained_at->toFormattedDateString() : "") . '</span>';
                    return $html;
                })
                ->addColumn('wo_no', function ($data) {
                    return $data->order_no;
                })
                // ->addColumn('attend', function ($data) {
                //     $html = '<span class="badge badge-light">' . (isset($data->attend->name) ? $data->attend->name : "") . ' </span>';
                //     $html .= ' <span class="badge badge-info">' . (isset($data->responded_at) ? $data->responded_at->toFormattedDateString() : "") . '</span>';
                //     return $html;
                // })
                ->addColumn('close', function ($data) {
                    $html = '<span class="badge badge-light">' . (isset($data->close->name) ? $data->close->name : "") . ' </span>';
                    $html .= ' <span class="badge badge-info">' . (isset($data->closed_at) ? $data->closed_at->toFormattedDateString() : "") . '</span>';
                    return $html;
                })
                ->setRowClass(function ($data) {
                    return $data->device->category->name === "Critical" ? 'alert-danger' : '';
                })
                ->addColumn('progress', function ($data) {
                    if ($data->progress === "completed")
                        $html = '<span class="badge badge-success">COMPLETED</span>';
                    elseif ($data->progress === "ongoing")
                        $html = '<span class="badge badge-warning">IN PROGRESS</span>';
                    else
                        $html = '<span class="badge badge-danger">PENDING</span>';

                    return $html;
                })
                ->rawColumns(['progress', 'actions', 'complaint', 'attend', 'close'])->make(true);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        try {
            return view('admin.complaint.index');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function create()
    {
        try {
            $html = "";
            $data = $this->service->getMetaData();
            $html .= view('admin.complaint.create', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display
     * @param $id
     * @throws \Throwable
     */
    public function show($id)
    {
        try {
            $html = "";
            $data['get'] = $this->service->get($id);
            $html .= view('admin.complaint.view', $data)->render();
            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Storing Complaint
     * @param StoreComplaint $request
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $request)
    {
        try {
            $this->service->store($request->except("_token"));
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('complaints.index')->withSuccess('New Complaint Added !!');
    }


    /**
     * API for getting a Single Complaint
     * @param $id
     * @throws \Throwable
     */
    // public function edit($id)
    // {
    //     try {
    //         $html = "";
    //         $data = $this->service->getMetaData();
    //         $data['get'] = $this->service->get($id);
    //         $data['users'] = $this->service->users($id);
    //         $html .= view('admin.complaint.edit', $data)->render();
    //         $response = ['status' => 201, 'data' => $html];
    //         echo json_encode($response);
    //     } catch (\Exception $e) {
    //         throw $e;
    //     }
    // }

    public function edit($id)
    {
        try {
            $data = $this->service->getMetaData();
            $data['get'] = $this->service->get($id);
            $data['users'] = $this->service->users($id);
            $data['comment'] = $this->service->comments($id);
            $data['spare_parts'] = $this->service->spares();
            $data['log'] = $this->service->getLog($id);
            $data['full_log'] = $this->service->getFullLog($id);

            $error_codes = [];
            if(!empty($data['errors'])){
                foreach($data['errors'] as $item){
                    $error_codes[$item->id] = $item->name;
                }
            }
            $data['error_codes'] = $error_codes;

            $userArray = [];
            if(!empty($data['users'])){
                foreach($data['users'] as $item){
                    $userArray[$item->id] = $item->name;
                }
            }
            $data['userArray'] = $userArray;
            return view('admin.complaint.edit', $data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * API for Updating a Single Complaint
     * @param StoreComplaint $request
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        try {
            $this->service->update($request->except("_token", "comment"), $id);
        } catch (\Exception $e) {
            throw $e;
        }
        # Return to the Ad listing view
        return Redirect::route('complaints.index')->withSuccess('New Complaint Added !!');
    }

    /**
     * API for Deleting a Complaints
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $this->service->destroy($id);
            $response = ['status' => 201, 'data' => 'deleted'];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    public function log($id)
    {
        $type = request()->get('type');
        try {
            $log = $this->service->log($id, $type);
            $users = $this->service->users($id);
            $get = $this->service->get($id);
            $html = '';

            $userArray = [];
            if(!empty($users)){
                foreach($users as $item){
                    $userArray[$item->id] = $item->name . ' ( '. $item->email .' ) ';
                }
            }

            if(!empty($log)){
                if($log[0]['status'] != 1){ 
                $html .= '<div class="alert alert-info">
                    <strong>User : </strong>';

                    if(!is_null($get->assigned_to)) { 
                        if(in_array(Auth::user()->id, json_decode($get->assigned_to))) {
                            $html .= $userArray[Auth::user()->id];
                        }
                    }
                $html .= '<br>
                    <strong>Total Time Worked : </strong>';
                    
                    if($log[0]['log_time']  < 60){
                        $html .= $log[0]['log_time']. ' Minutes';
                    }
                    else{
                        $hours = (int)($log[0]['log_time']/60);
                        $minutes = $log[0]['log_time'] % 60;

                        $html .= $hours. ' Hour '.$minutes.' Minutes';
                    }
                $html .= '</div>';
                }
            }

            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    public function comment($id)
    {
        $comment = request()->get('comment');

        try {
            $result = $this->service->comment($id, $comment);
            $users = $this->service->users($id);
            $get = $this->service->get($id);

            $userArray = [];
            if(!empty($users)){
                foreach($users as $item){
                    $userArray[$item->id] = $item->name;
                }
            }

            $html = '';

            if(!empty($result)){
                foreach($result as $item){
                    $class_1 = '';
                    $class_2 = 'my-message';
                    if($item['user_id'] == Auth::user()->id){
                        $class_1 = 'text-right';
                        $class_2 = 'other-message float-right';
                    }

                    $html .= '<li class="clearfix">
                                <div class="status online message-data '.$class_1.'">';
                    $html .= '<span class="name">'.$userArray[$item['user_id']].'</span>';
                    $html .= '<span class="time">'.date('d-m-Y h:i A', strtotime($item['created_at'])).'</span>';
                    $html .= '<i class="zmdi zmdi-circle me"></i>
                            </div>';
                    $html .= '<div class="message '.$class_2.'"> '. $item['comment'] .'</div>';
                }
            }

            $response = ['status' => 201, 'data' => $html];
            echo json_encode($response);

        }catch (\Exception $e) {
            throw $e;
        }

    }


    public function progress($id)
    {
        $progress = request()->get('progress');

        try {
                $this->service->progress($id, $progress);
                $response = ['status' => 201, 'data' => 'success'];
                echo json_encode($response);

        }catch (\Exception $e) {
            throw $e;
        }

    }

}

