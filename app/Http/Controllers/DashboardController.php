<?php

namespace App\Http\Controllers;

use App\Services\DashboardService;

class DashboardController extends Controller
{
    private $service;

    public function __construct(DashboardService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        try {
            $data = $this->service->getData();
            return view('admin.dashboard.index', $data);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
