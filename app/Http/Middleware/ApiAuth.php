<?php


namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class ApiAuth
{
    public function handle($request, Closure $next)
    {
        if (User::where('api_token', $request->bearerToken())->count() === 0) {
            return response()->json(['success' => false, 'data' => 'Invalid Credentials'], 401);
        }
        return $next($request);
    }
}
