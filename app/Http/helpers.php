<?php
/**
 * @param $data
 * @param bool $die
 */

function pr($data, $die = false)
{

    echo '<pre>';
    print_r($data);
    echo '</pre>';

    if ($die) {
        die;
    }
}

/**
 * @param $length
 * @return string
 */
function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet .= "0123456789";
    $max = strlen($codeAlphabet);

    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[random_int(0, $max - 1)];
    }

    return $token;
}


/**
 * @return mixed|null
 */
function defaultCompany()
{
    return \DB::table('companies')->value('id');
}

/**
 * Get Slug
 * @param $text
 * @return bool|null|string|string[]
 */
function slugify($text)
{
    # replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    # transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    # remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    # trim
    $text = trim($text, '-');
    # remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    # lowercase
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}
