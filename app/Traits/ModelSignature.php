<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait ModelSignature
{
    protected static function boot()
    {
        parent::boot();

        static::updating(function ($model) {

            $model->updated_by = Auth::check() ? Auth::User()->id : -1;

        });

        static::creating(function ($model) {

            $model->updated_by = Auth::check() ? Auth::User()->id : -1;

        });

        static::deleting(function ($model) {

            $model->updated_by = Auth::check() ? Auth::User()->id : -1;

        });
    }

}