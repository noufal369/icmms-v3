<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer(['layouts.nav'], function ($view) {
            $user = User::find(\Auth::user()->id);
            if($user->user_type_id === 1) {
                $activeCompany = session('company', Company::value('name'));
            } else {
                $activeCompany = Company::find($user->department->company_id);
                session(['company' => $activeCompany->name, 'company_id' => $activeCompany->id]);
            }
            $view->with([
                'companies' => Company::all(),
                'activeCompany' => $activeCompany,
                'type' => $user->user_type_id
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
