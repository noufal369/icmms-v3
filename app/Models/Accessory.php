<?php

namespace App\Models;

use App\Traits\ModelSignature;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accessory extends Model
{
    use SoftDeletes;
    use ModelSignature;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'model_no',
        'serial_no',
        'manufacturer_id',
        'stock',
        'comments',
        'status_id',
        'updated_by',
        'date',
        'sku',
        'cost',
        'image',
        'qr_code',
        'warranty_date',
    ];

}
