<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ComplaintComments extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected  $dates = ['complained_at', 'responded_at', 'closed_at'];

    # Scope for fetch
    public function scopeFetch($query)
    {
        return $query->with(
            'device.department.company',
            'attend',
            'close',
            'error'
        );
    }

    # Scope for Company based fetch
    public function scopeCompanyFetch($query)
    {
        return $query->whereHas('device.department', function ($q) {
            $q->where('company_id', session('company_id', defaultCompany()));
        });
    }


    # Relation with Color Collection
    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    # Relation with User
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'complained_by');
    }

    # Relation with User
    public function attend()
    {
        return $this->hasOne(User::class, 'id', 'responded_by');
    }

    # Relation with User
    public function close()
    {
        return $this->hasOne(User::class, 'id', 'closed_by');
    }

    # Relation with Error
    public function error()
    {
        return $this->belongsTo(Error::class);
    }
}
