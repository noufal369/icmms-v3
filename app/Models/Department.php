<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Department
 *
 * @property-read \App\Models\Company $company
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $company_id
 * @property int $status_id
 * @property int $updated_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department whereUpdatedBy($value)
 */
class Department extends Model
{
	protected $fillable = [
        'name',
        'company_id',
        'status_id',
        'updated_by'
    ];

    # Scope for Company based fetch
    public function scopeCompanyFetch($query)
    {
        return $query->where('company_id', session('company_id', defaultCompany()));
    }

    # Relation with Color Collection
    public function company() {
        return $this->belongsTo(Company::class);
    }


}
