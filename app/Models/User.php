<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelSignature;

class User extends Model
{
    use SoftDeletes;
    use ModelSignature;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'department_id',
        'user_type_id',
        'staff_id',
        'status_id',
        'updated_by',
        'company_id',
        'extension_number',
        'phone',
        'address',
    ];

    # Relation with Department
    public function userType()
    {
        return $this->belongsTo(UserType::class);
    }

    # Relation with Department
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    # Scope for Company based fetch
    public function scopeCompanyFetch($query)
    {
        return $query->whereHas('department', function ($q) {
            $q->where('company_id', session('company_id', defaultCompany()));
        });
    }
}
