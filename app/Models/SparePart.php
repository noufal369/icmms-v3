<?php

namespace App\Models;

use App\Traits\ModelSignature;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SparePart
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $location
 * @property string $email
 * @property string $phone
 * @property int $status_id
 * @property int $updated_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SparePart whereUpdatedBy($value)
 */
class SparePart extends Model
{
    use SoftDeletes;
    use ModelSignature;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'model_no',
        'serial_no',
        'manufacturer',
        'stock',
        'date',
        'other_info',
        'status_id',
        'updated_by',
        'sku',
        'cost',
        'image',
        'qr_code',
        'warranty_date',
    ];

}
