<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'department_id',
        'serial_no',
        'model_no',
        'gmdn',
        'supplier_id',
        'manufacturer_id',
        'service_provider_id',
        'warranty',
        'amc_start',
        'cmc_start',
        'amc',
        'cmc',
        'pm',
        'purchased_at',
        'purchase_order_no',
        'installed_at',
        'purchase_cost',
        'accessories',
        'firmware_version',
        'ownership',
        'inspection_details',
        'calibration_at',
        'pm_frequency',
        'calibration_frequency',
        'category_id',
        'description_pdf',
        'applicable_device',
        'adverse_events',
        'other_info',
        'power_spec',
        'image',
        'qr_code',
        'comments',
        'checklist',
        'spare_parts',
        'condition',
        'status_id',
        'updated_by',
        'preferred_engineer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
//     protected $hidden = [
//          'password', 'remember_token',
//     ];

    # Scope for fetch
    public function scopeFetch($query)
    {
        return $query->with(
            'department.company',
            'service_provider',
            'manufacturer',
            'supplier',
            'status',
            'category',
            'complaints'
        );
    }

    # Scope for fetch
    public function scopeFetchMin($query)
    {
        return $query->with(
            'department.company'
        );
    }

    # Scope for Company based fetch
    public function scopeCompanyFetch($query)
    {
        return $query->whereHas('department', function ($q) {
            $q->where('company_id', session('company_id', defaultCompany()));
        });
    }


    # Relation with Department
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    # Relation with Service Provider
    public function service_provider()
    {
        return $this->belongsTo(ServiceProvider::class);
    }

    # Relation with Manufacturer
    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    # Relation with Supplier
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    # Relation with Status
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    # Relation with Status
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    # Relation with Status
    public function complaints()
    {
        return $this->hasMany(Complaint::class);
    }


}
