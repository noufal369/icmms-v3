<?php

namespace App\Models;

use App\Traits\ModelSignature;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Tutorial
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $location
 * @property string $email
 * @property string $phone
 * @property int $status_id
 * @property int $updated_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tutorial whereUpdatedBy($value)
 */
class Tutorial extends Model
{
    use SoftDeletes;
    use ModelSignature;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device',
        'search_tags',
        'description',
        'file',
        'status_id',
        'updated_by'
    ];

}
