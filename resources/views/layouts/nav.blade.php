<style type="text/css">
    .sidebar .menu .list a {
        padding: 13px 10px;
    }
</style>

<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{route('dashboard')}}"><img src="{{ URL::to('/') }}/assets/images/favicon.ico" width="50" alt="Aero"><span class="m-l-10">ICMMS</span></a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <div class="image"><a href="#"><img src="{{ URL::to('/') }}/assets/images/avatar.png" alt="User"></a></div>
                    <div class="detail">
                        <h4>{{Auth::user()->name}}</h4>
                        <!-- <small>Super Admin</small> -->
                    </div>
                </div>
            </li>            
            <li class="{{ Request::segment(1) === 'dashboard' ? 'active open' : null }}"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li class="{{ Request::segment(1) === 'devices' ? 'active open' : null }}"><a href="{{route('devices.index')}}"><i class="zmdi zmdi-card-membership"></i><span>Equipments</span></a></li>
            <li class="{{ Request::segment(1) === 'complaints' ? 'active open' : null }}"><a href="{{route('complaints.index')}}"><i class="zmdi zmdi-assignment"></i><span>Tickets</span></a></li>
            <li class="{{ Request::segment(1) === 'pm' ? 'active open' : null }}"><a href="{{ route('pm.index') }}"><i class="zmdi zmdi-wrench"></i><span>Pm</span></a></li>

            <li class="{{ Request::segment(1) === 'checklist' || Request::segment(1) === 'suppliers' || Request::segment(1) === 'manufacturers' || Request::segment(1) === 'service_providers' || Request::segment(1) === 'spare_parts' || Request::segment(1) === 'accessories' || Request::segment(1) === 'departments' ? 'active open' : null }}">
                <a href="#Project" class="menu-toggle"><i class="zmdi zmdi-settings"></i> <span>Settings</span></a>
                <ul class="ml-menu">
                    <li class="{{ Request::segment(1) === 'checklist' ? 'active' : null }}"><a href="{{ route('checklist.index') }}">Pm Checklist</a></li>
                    <li class="{{ Request::segment(1) === 'suppliers' ? 'active' : null }}"><a href="{{ route('suppliers.index') }}">Suppliers</a></li>
                    <li class="{{ Request::segment(1) === 'manufacturers' ? 'active' : null }}"><a href="{{ route('manufacturers.index') }}">Manufacturer</a></li>                                    
                    <li class="{{ Request::segment(1) === 'service_providers' ? 'active' : null }}"><a href="{{ route('service_providers.index') }}">Service Provider</a></li>
                    <li class="{{ Request::segment(1) === 'spare_parts' ? 'active' : null }}"><a href="{{ route('spare_parts.index') }}">Spare Parts</a></li>
                    <li class="{{ Request::segment(1) === 'accessories' ? 'active' : null }}"><a href="{{ route('accessories.index') }}">Accessories</a></li>
                    <li class="{{ Request::segment(1) === 'departments' ? 'active' : null }}"><a href="{{ route('departments.index') }}">Departments</a></li>
                </ul>
            </li>
            <li class="{{ Request::segment(1) === 'tutorials' ? 'active open' : null }}"><a href="{{ route('tutorials.index') }}"><i class="zmdi zmdi-movie"></i><span>Tutorials</span></a></li>
            <li class="{{ Request::segment(1) === 'users' ? 'active open' : null }}"><a href="{{ route('users.index') }}"><i class="zmdi zmdi-account"></i><span>Users</span></a></li>

            <li class="{{ Request::segment(1) === 'report' ? 'active open' : null }}"><a href="{{ route('report.index') }}"><i class="zmdi zmdi-library"></i><span>Report</span></a></li>


            @if($type === 1)
            <li>
                <li class="btn-group">
                    <li class="btn bg-pink"> {{ $activeCompany }}</li>
                    <li class="btn dropdown-toggle dropdown-toggle-split"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    </li>
                    <li class="dropdown-menu">
                        @foreach($companies as $company)
                            <a class="dropdown-item"
                                href="{{ route('changeCompany', $company->id) }}">{{ $company->name }}
                            </a>
                        @endforeach
                    </li>
                </li>
            </li>
            @endif

        </ul>
    </div>
</aside>
