<p>
    Hi <span style="text-transform: capitalize">{{ $name ?? '' }} ,</span> <br><br>

    The following task has been assigned to you. <br><br>
    Work Order No: <b>{{ $woNo ?? '' }}</b> <br>
    Complaint: <b>{{ $complaint ?? '' }}</b> <br>
    Date: <b> {{ now()->format('d-m-Y H:i:s') }}</b> <br>
    Customer: <b>{{ $customer ?? '' }}</b><br> <br>

    For More details : <a href="{{ $link }}">Click here</a>


</p>
<p><i>THIS IS AN AUTOMATED MESSAGE - PLEASE DO NOT REPLY DIRECTLY TO THIS EMAIL</i></p>
<br><br>
<p>Team iCMMS</p>
