@extends('layouts.app')
@section('title', 'Edit Ticket')
@section('page-style')
@section('content')

<link rel="stylesheet" href="{{asset('assets/plugins/multi-select/css/multi-select.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.css')}}"/>

<script src="{{asset('assets/plugins/multi-select/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/plugins/quick-search/js/jquery.quicksearch.js')}}"></script>

<style>
.progress .progress-bar{
        height: 15px;
    }
.custom-header {
    text-align: center;
    padding: 3px;
    background: #1cbfd0;
    color: #fff;
    font-size: 15px;
    height: 35px;
    }
.sweet-alert h2 {
    margin: 30px 0px 5px 0px !important;
}
.my-data div{
    float:left;
    width:50%;
    /*text-align:center;*/
    position:relative;
    height: 30px;
}
.badge{
    font-size: 12px;
    padding: 6px 10px;
}
</style>
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="product details">
                            <h3 class="product-title mb-0">{{$get->title}} 
                                <button class="btn btn-sm btn-danger waves-effect" data-toggle="modal" data-target="#engineerModal" style="float: right;"><i class="zmdi zmdi-account-add" style="font-size: 21px;"></i></button>
                            </h3><br>

                            <h5 class="price mt-0">Work Order No : <span class="col-purple">{{ $get->order_no }}</span></h5>
                            <h5 class="price mt-0">Severity :                                  
                                @if($get->severity == 0)
                                    <span class="col-green">Minor</span>
                                @elseif($get->severity == 1)
                                    <span class="col-amber">Major</span>
                                @elseif($get->severity == 2)
                                    <span class="col-red">Critical</span>
                                @endif
                            </h5>

                            
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-sm  {{ 'pending' == $get->progress ? 'btn-danger' : 'btn-secondary'}} pending" onclick="changeProgress('pending')">
                                    <input type="radio" name="progress" id="option1" value="pending" 
                                        autocomplete="off" {{ 'pending' == $get->progress ? 'checked' : ''}}>
                                        PENDING
                                </label>
                                <label class="btn  btn-sm  {{ 'ongoing' == $get->progress ? 'btn-warning' : 'btn-secondary'}} in-progress" onclick="changeProgress('ongoing')">
                                    <input type="radio" name="progress" id="option2" value="ongoing" 
                                        autocomplete="off" {{ 'ongoing' == $get->progress ? 'checked' : ''}}>
                                        IN PROGRESS
                                </label>
                                <label class="btn  btn-sm {{ 'completed' == $get->progress ? 'btn-success' : 'btn-secondary'}} completed" onclick="changeProgress('completed')">
                                    <input type="radio" name="progress" id="option3" value="completed" 
                                        autocomplete="off" {{ 'completed' == $get->progress ? 'checked' : ''}}>
                                        COMPLETED
                                </label>
                            </div>

                            @if(!empty($get->assigned_to))
                                @if(in_array(Auth::user()->id, json_decode($get->assigned_to)))

                                            
                                <div class="btn-group" id="btn-group" style="float: right;">                                                    
                                    <div id="start-btn">
                                    @if(empty($log))
                                    <button type="button" class="btn btn-success" id="start" onclick="time_log({{$get->id}}, 1)"><i class="fa fa-hourglass-start"></i><span style="margin-left: 10px;">START</span></button>
                                    @endif
                                    </div>                                                    
                                    <div id="pause-btn">
                                    @if(!empty($log))
                                    @if($log[0]['status'] == 1 || $log[0]['status'] == 3)
                                    <button type="button" class="btn btn-warning" id="pause" onclick="time_log({{$get->id}}, 2)"><i class="fa fa-pause"></i><span style="margin-left: 10px;">PAUSE</span></button>
                                    @endif
                                    @endif
                                    </div>
                                                    
                                    <div id="resume-btn">
                                    @if(!empty($log))
                                    @if($log[0]['status'] == 2)
                                    <button type="button" class="btn btn-info" id="resume" onclick="time_log({{$get->id}}, 3)"><i class="fa fa-play"></i><span style="margin-left: 10px;">RESUME</span></button>
                                    @endif
                                    @endif
                                    </div>

                                    <div id="stop-btn">
                                    @if(!empty($log) && $log[0]['status'] != 4)
                                    <button type="button" class="btn btn-danger" id="stop" onclick="time_log({{$get->id}}, 4)"><i class="fa fa-stop-circle"></i><span style="margin-left: 10px;">STOP</span></button>
                                    @endif
                                    </div>
                                                    
                                    </div>                                             
                                            
                                @endif
                            @endif


                            <div id="alert-div" style="margin-top: 20px;">
                            @if(!empty($log)) 
                            @if($log[0]['status'] != 1 && $log[0]['status'] != 3)
                                <div class="alert alert-info">
                                    <strong>User : </strong>
                                                    
                                <?php if(!is_null($get->assigned_to)) { 
                                    if(in_array(Auth::user()->id, json_decode($get->assigned_to))) { ?>
                                            {{Auth::user()->name}}

                                <?php }
                                } ?>
                                                    
                                <br>
                                <strong>Total Time Worked : </strong>
                                <?php
                                if($log[0]['log_time']  < 60){
                                    echo $log[0]['log_time']. ' Minutes';
                                }
                                else{
                                    $hours = (int)($log[0]['log_time']/60);
                                    $minutes = $log[0]['log_time'] % 60;

                                    echo $hours. ' Hour '.$minutes.' Minutes';
                                }
                                ?>
                            </div>
                            @endif 
                            @endif
                            </div>
                                            
                                    

                            <hr>
                            <p class="product-description">
                                <div class="my-data">
                                    <div class="area">
                                        <strong>Device : </strong>
                                        {{$get->device->name}}
                                    </div>
                                    <div class="area">
                                        <strong>Error Code : </strong>
                                        @foreach ($errors as $value)
                                            @if($value->id == $get->error_id)
                                                {{ $value->name }}
                                            @endif
                                        @endforeach
                                        <span></span>
                                    </div>
                                    <div class="area">
                                        <strong>Reported On : </strong>{{$get->complained_at->toFormattedDateString()}}
                                    </div>
                                    <div class="area">
                                        <strong>Reported By : </strong>
                                        {{ $userArray[$get->complained_by] }}
                                    </div>
                                    <div class="area">
                                        <strong>Responded At : </strong>
                                        {{ isset($get->responded_at)? $get->responded_at->toFormattedDateString() : ''}}
                                    </div>
                                    <div class="area">
                                        <strong>Responded By : </strong>
                                        {{ isset($get->responded_by)? $userArray[$get->responded_by] : ''}}
                                    </div>
                                    <div class="area">
                                        <strong>Assigned To : </strong>
                                        @if(!is_null($get->assigned_to))
                                            @foreach ($users as $value)
                                                @if(in_array($value->id, json_decode($get->assigned_to)))
                                                    <span class="badge badge-info">{{$value->name}}</span>
                                                @endif  
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="area">
                                        <strong>Spare Parts : </strong>
                                        @if(!is_null($get->spare_part_id))
                                            @foreach($spare_parts as $item)
                                               @if(in_array($item->id, json_decode($get->spare_part_id)))
                                                    {{$item->name}} , &nbsp;
                                               @endif
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="area">
                                        <strong>Closed On : </strong>
                                        {{ isset($get->closed_at)? $get->closed_at->toFormattedDateString() : ''}}
                                    </div>
                                    <div class="area">
                                        <strong>Closed By : </strong>
                                        {{ isset($get->closed_by)? $userArray[$get->closed_by] : ''}}
                                    </div>
                                    <div class="area">
                                        <strong>Expected Completion Time : </strong>
                                        {{ $get->expected_completion_time }}
                                    </div>
                                    <div class="area">
                                        <strong>Reason For Pending ( If Any ) : </strong>
                                        {{ $get->reason_for_pending }}
                                    </div>
                                    <div class="area">
                                        <strong>Cost Involved : </strong>
                                        {{ $get->cost }}
                                    </div>
                                    <div class="area">
                                        <strong>Customer : </strong>
                                        {{isset($get->device->department->name)? $get->device->department->name : ''}}
                                    </div>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item comment">
                                    <a class="nav-link active" data-toggle="tab" href="#commentPanel" role="tab">Comments</a>
                                <li>
                                @if(Auth::user()->user_type_id == 1)
                                <li class="nav-item">
                                    <a class="nav-link log" data-toggle="tab" href="#logPanel" role="tab">Engineer Log</a>
                                <li>
                                @endif
                                <li class="nav-item basic">
                                    <a class="nav-link" data-toggle="tab" href="#infoPanel" role="tab">Basic
                                        Information</a>
                                <li>
                                <li class="nav-item options">
                                    <a class="nav-link" data-toggle="tab" href="#options" role="tab">Options</a>
                                <li>
                                <li class="nav-item attach">
                                    <a class="nav-link" data-toggle="tab" href="#placementPanel"
                                       role="tab">Attachments</a>
                                <li>
                            </ul><hr>

                            <form class="form-group company-form" role="form" method="POST" action="{{ route('complaints.update', $get->id) }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <div class="tab-content mt-2">

                                    <div class="tab-pane fade show active" id="commentPanel" role="tabpanel">
                                        <div class="chat_window body" style="margin-left: 0px;">
                                        <ul class="chat-history">
                                            @if(!empty($comment))
                                            @foreach ($comment as $value)
                                            <li class="clearfix">
                                                <div class="status online message-data @if($value['user_id'] == Auth::user()->id) text-right @endif">
                                                    <span class="name">{{$userArray[$value['user_id']]}}</span>
                                                    <span class="time">{{date('d-m-Y h:i A', strtotime($value['created_at']))}}</span>
                                                    <i class="zmdi zmdi-circle me"></i>
                                                </div>
                                                <div class="message @if($value['user_id'] == Auth::user()->id) other-message float-right @else my-message @endif"> 
                                                    {{$value['comment']}} 
                                                </div>
                                            </li>
                                            @endforeach
                                            @endif
                                        </ul>
                                        <div class="chat-box">
                                            <div class="input-group">
                                                
                                                <input id="comment" name="comment" type="text" class="form-control" placeholder="Add Comment...">
                                                <div class="input-group-prepend">
                                                    <button type="button" class="input-group-text" onclick="addComment()"><i class="zmdi zmdi-mail-send"></i></button>
                                                </div>
                                            </div>                                                            
                                        </div>
                                    </div>
                                    </div>

                                    <div class="tab-pane fade" id="logPanel" role="tabpanel">
                                    <br>

                                    <div class="card">
                                        <div class="body">
                                            @if(!empty($full_log))
                                            <ul class="list-unstyled activity">
                                                @foreach($full_log as $item)
                                                <li class="a_birthday">
                                                    <h4>{{ $userArray[$item['user_id']] }}</h4>
                                                    <p>Total Time Worked : 
                                                    <?php
                                                        if($item['log_time']  < 60){
                                                            echo $item['log_time']. ' Minutes';
                                                        }
                                                        else{
                                                            $hours = (int)($item['log_time']/60);
                                                            $minutes = $item['log_time'] % 60;

                                                            echo $hours. ' Hour '.$minutes.' Minutes';
                                                        }
                                                    ?>
                                                    </p>
                                                    <small>Started At : {{ \Carbon\Carbon::parse($item['created_at'])->toFormattedDateString() }} {{ date('h:i A', strtotime($item['created_at'])) }}</small>
                                                </li>
                                                @endforeach
                                            </ul>
                                            @else
                                            <div class="alert alert-danger">
                                                <strong>Oh Snap !</strong> No Engineer Log Found.
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    </div>


                                    <div class="tab-pane fade" id="infoPanel" role="tabpanel">
                                        <br>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Title*</label>
                                                <input type="text" name="title" value="{{$get->title}}"
                                                       class="form-control" placeholder="Title" disabled="disabled"
                                                       max="50">
                                            </div>
                                            <div class="col">
                                                <label for="error_id" class="control-label">Error Code*</label>
                                                <select class="form-control ms" name="error_id" id="error_id">
                                                    <option value="" selected>Select Error Code</option>
                                                    @foreach ($errors as $value)
                                                        <option value="{{ $value->id }}" {{ $value->id == $get->error_id ? 'selected="selected"' : ''}}>
                                                            {{ $value->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Cost</label>
                                                <input type="number" name="cost" value="{{$get->cost}}"
                                                       class="form-control"
                                                       placeholder="Cost">
                                            </div>
                                        </div>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Severity</label>
                                                <select class="form-control" name="severity" id="severity">
                                                    
                                                    <option value="0" {{ 0 == $get->severity ? 'selected="selected"' : ''}}>
                                                            Minor
                                                    </option>
                                                    <option value="1" {{ 1 == $get->severity ? 'selected="selected"' : ''}}>
                                                            Major
                                                    </option>
                                                    <option value="2" {{ 2 == $get->severity ? 'selected="selected"' : ''}}>
                                                            Critical
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-row col-sm-12">
                                            <!-- <div class="col">
                                                <label class="control-label">Expected Completion Time</label>
                                                <textarea class="form-control" rows="3" name="expected_completion_time"
                                                          placeholder="Expected Completion Time"
                                                          maxlength="300">{{$get->expected_completion_time}}</textarea>
                                            </div> -->
                                            <div class="col">
                                                <label class="control-label">Reason For Pending</label>
                                                <textarea class="form-control" rows="3" name="reason_for_pending"
                                                          placeholder="Reason For Pending"
                                                          maxlength="300">{{$get->reason_for_pending}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">More Info</label>
                                                <textarea class="form-control" rows="3" name="more_info"
                                                          placeholder="More Info"
                                                          maxlength="300">{{$get->more_info}}</textarea>
                                            </div>
                                            <div class="col">
                                                <label class="control-label">Reason for Reassign</label>
                                                <textarea class="form-control" rows="3" name="reason_for_reassign"
                                                          placeholder="Reason for Reassign"
                                                          maxlength="300">{{$get->reason_for_reassign}}</textarea>
                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-secondary pull-right" id="infoContinue">Continue</button>
                                    </div>

                                    <div class="tab-pane fade" id="options" role="tabpanel"><br>

                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label for="control-label">Estimated Time</label>
                                                <input type="text" name="expected_completion_time" class="form-control"
                                                       id="expected_completion_time" value="{{$get->expected_completion_time}}" {{ Auth::user()->user_type_id != '1'? 'disabled="disabled"' : ''}}>
                                            </div>  
                                        </div>

                                        <div class="form-group form-row col-sm-12" style="margin-top: 20px;">
                                            <div class="col">
                                                <label for="control-label">Spare Parts Used</label>
                                                <select name="spare_part_id[]" id="spare_part_id" class="form-control ms" multiple="multiple">
                                                    @foreach($spare_parts as $item)
                                                    <option value="{{$item['id']}}" <?php if(!is_null($get->spare_part_id)) { if(in_array($item->id, json_decode($get->spare_part_id))) { ?> selected="selected"  <?php } } ?>>{{$item['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>  
                                        </div>

                                        <br>
                                        <button class="btn btn-secondary pull-right" id="optContinue">Continue</button>
                                    </div>

                                    <div class="tab-pane fade" id="placementPanel" role="tabpanel"><br>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label for="exampleInputFile">Document</label>
                                                <input type="file" name="document" class="form-control-file"
                                                       id="exampleInputFile" aria-describedby="fileHelp">
                                                <small id="fileHelp" class="form-text text-muted">
                                                    Select a file to use as the fullscreen ad image. Please ensure the
                                                    size is at least 1080x1920 with a 9:16 (portrait) aspect ratio.
                                                </small>
                                            </div>
                                            <div class="col">
                                                <label for="exampleInputFile">Photo</label>
                                                <input type="file" name="image" class="form-control-file"
                                                       id="exampleInpuImage"
                                                       aria-describedby="fileHelp">
                                                <small id="fileHelp" class="form-text text-muted">
                                                    Select a file to use as the banner ad image. Please ensure the size
                                                    is exactly 1080x450 for proper rendering.
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-5" style="display: none;">
                                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width: 33%" aria-valuenow="20"
                                     aria-valuemin="0" aria-valuemax="100">Step 1 of 3
                                </div>
                            </div>
                            <div class="modal-footer update-report" style="display: none;">
                                <button type="submit" class="btn btn-info btn-round" id="activate">Update Report
                                </button>
                                <!-- <button type="button" class="btn btn-secondary" data-backdrop="false" data-dismiss="modal">Close</button> -->
                            </div>
                        </form>
                        </div>

                    </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="engineerModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form-group company-form" role="form" method="POST" action="{{ route('complaints.update', $get->id) }}"
                enctype="multipart/form-data" autocomplete="off">
                @csrf
                @method('PUT')
            <div class="modal-header bg-info">
                <h4 class="title" id="largeModalLabel" style="color: #fff;">Assign Engineers</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-row col-sm-12">
                    <div class="col">
                        <label class="control-label">Assigned To</label>
                        <select class="form-control show-tick ms select2" name="assigned_to[]" id="assigned_to" multiple="multiple" data-placeholder="Select" style="height: calc(1.5em + .75rem + 10px);">
                                @foreach ($users as $value)
                                <option value="{{ $value->id }}" <?php if(!is_null($get->assigned_to)) { if(in_array($value->id, json_decode($get->assigned_to))) { ?> selected="selected"  <?php } } ?>>
                                    {{ $value->name }}
                                </option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group form-row col-sm-12">
                </div>
                <div class="form-group form-row col-sm-12">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info btn-round waves-effect">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger btn-round waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
            </form>
        </div>
    </div>
</div>



<script>
    $(function () {
        $('#infoContinue').click(function (e) {
            e.preventDefault();
            $('.progress-bar').css('width', '66%');
            $('.progress-bar').html('Step 2 of 3');
            $('#myTab a[href="#options"]').tab('show');
        });

        $('#optContinue').click(function (e) {
            e.preventDefault();
            $('.progress-bar').css('width', '100%');
            $('.progress-bar').html('Step 3 of 3');
            $('#myTab a[href="#placementPanel"]').tab('show');
        });
    })

</script>

<script>
    $(function () {
        $('.comment').click(function (e) {
            e.preventDefault();
            $('.progress').hide();
            $('.update-report').hide();
        });

        $('.log').click(function (e) {
            e.preventDefault();
            $('.progress').hide();
            $('.update-report').hide();
        });

        $('.basic').click(function (e) {
            e.preventDefault();
            $('.progress').show();
            $('.update-report').show();
        });

        $('.option').click(function (e) {
            e.preventDefault();
            $('.progress').show();
            $('.update-report').show();
        });

        $('.attach').click(function (e) {
            e.preventDefault();
            $('.progress').show();
            $('.update-report').show();
        });
    })

</script>

<script>
    $('.pending').click(function(){
        $('.pending').removeClass('btn-secondary');
        $('.pending').addClass('btn-danger');
        $('.in-progress').removeClass('btn-warning');
        $('.completed').removeClass('btn-success');
        $('.in-progress').addClass('btn-secondary');
        $('.completed').addClass('btn-secondary');
    });
    $('.in-progress').click(function(){
        $('.in-progress').removeClass('btn-secondary');
        $('.pending').removeClass('btn-danger');
        $('.in-progress').addClass('btn-warning');
        $('.completed').removeClass('btn-success');
        $('.pending').addClass('btn-secondary');
        $('.completed').addClass('btn-secondary');
    });
    $('.completed').click(function(){
        $('.completed').removeClass('btn-secondary');
        $('.pending').removeClass('btn-danger');
        $('.in-progress').removeClass('btn-warning');
        $('.completed').addClass('btn-success');
        $('.pending').addClass('btn-secondary');
        $('.in-progress').addClass('btn-secondary');
    });
</script>

<script type="text/javascript">
        $('#spare_part_id').multiSelect({
            selectableHeader: "<div class='custom-header'>Available</div>",
            selectionHeader: "<div class='custom-header'>Selected</div>"
        });

        $('#reqBtn').click(function(e){
            e.preventDefault();
            $('.req_area').toggle();
        });
</script>

<script>  
  $(".select2").select2();
</script>

<script>
    function addComment() {
        var device;
        var comment = $('#comment').val();
            if (comment == '')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "{{ route('complaints.comment', $get->id) }}",
                dataType: 'JSON',
                data: { comment:comment },
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {
                    $('.chat-history').empty();
                    $('#comment').val('');
                    $('.chat-history').append(response.data);
                }
            });
        }
</script>

<script>
    function changeProgress(progress)
    {
        var msgTxt;
        var msgTitle;
            
        msgTxt = "Done !!!";
        msgTitle = "Are you sure you want to continue ?";

        swal({
                title: msgTitle,
                text: "",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                setTimeout(function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        url: "{{ route('complaints.progress', $get->id) }}",
                        data: { progress:progress },
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            if (response.status === 201) {
                                swal({title: msgTxt, type: "success"});
                            } else {
                                swal({title: "Process failed", type: "error"});
                            }
                        }
                    });
                }, 1000);
            });

    }
</script>

<script>
    function time_log(id, ele) {
            var msgTxt;
            var msgTitle;

            if(ele == 1){
                msgTxt = "Started !!!";
                msgTitle = "Are you sure you want to start ?";
            }else if(ele == 2){
                msgTxt = "Paused !!!";
                msgTitle = "Are you sure you want to pause ?";
            }else if(ele == 3){
                msgTxt = "Resumed !!!";
                msgTitle = "Are you sure you want to resume ?";
            }else if(ele == 4){
                msgTxt = "Stopped !!!";
                msgTitle = "Are you sure you want to stop ?";
            }
            
            
            swal({
                title: msgTitle,
                text: "",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                setTimeout(function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        url: "{{ route('complaints.log', $get->id) }}",
                        data: { type:ele },
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            if (response.status === 201) {
                                if(ele == 1){
                                    $('#start-btn').empty();
                                    $('#resume-btn').empty();
                                    $('#pause-btn').empty();
                                    $('#stop-btn').empty();
                                    $('#start-btn').append('<button type="button" class="btn btn-warning" id="pause" onclick="time_log({{$get->id}}, 2)"><i class="fa fa-pause"></i><span style="margin-left: 10px;">PAUSE</span></button><button type="button" class="btn btn-danger" id="stop" onclick="time_log({{$get->id}}, 4)"><i class="fa fa-stop-circle"></i><span style="margin-left: 10px;">STOP</span></button>');
                                    
                                }
                                if(ele == 2){
                                    $('#start-btn').empty();
                                    $('#resume-btn').empty();
                                    $('#pause-btn').empty();
                                    $('#stop-btn').empty();
                                    $('#resume-btn').append('<button type="button" class="btn btn-info" id="resume" onclick="time_log({{$get->id}}, 3)"><i class="fa fa-play"></i><span style="margin-left: 10px;">RESUME</span></button><button type="button" class="btn btn-danger" id="stop" onclick="time_log({{$get->id}}, 4)"><i class="fa fa-stop-circle"></i><span style="margin-left: 10px;">STOP</span></button>');
                                    $('#alert-div').empty();
                                    $('#alert-div').append(response.data);
                                }
                                if(ele == 3){
                                    $('#start-btn').empty();
                                    $('#resume-btn').empty();
                                    $('#pause-btn').empty();
                                    $('#stop-btn').empty();
                                    $('#pause-btn').append('<button type="button" class="btn btn-warning" id="pause" onclick="time_log({{$get->id}}, 2)"><i class="fa fa-pause"></i><span style="margin-left: 10px;">PAUSE</span></button><button type="button" class="btn btn-danger" id="stop" onclick="time_log({{$get->id}}, 4)"><i class="fa fa-stop-circle"></i><span style="margin-left: 10px;">STOP</span></button>');
                                    $('#alert-div').empty();
                                }
                                if(ele == 4){                                    
                                    $('#btn-group').remove();
                                    $('#alert-div').empty();
                                    $('#alert-div').append(response.data);
                                }
                                
                                swal({title: msgTxt, type: "success"});
                            } else {
                                swal({title: "Process failed", type: "error"});
                            }
                        }
                    });
                }, 1000);
            });
        }
</script>

@endsection
@section('footer_scripts')