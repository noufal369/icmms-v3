<style>
    /* Custom By Noufal */

    .preview {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column; }
    @media screen and (max-width: 996px) {
        .preview {
            margin-bottom: 20px; } }

    .preview-pic {
        -webkit-box-flex: 1;
        -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
        flex-grow: 1; }

    .preview-thumbnail.nav-tabs {
        border: none;
        margin-top: 15px; }
    .preview-thumbnail.nav-tabs li {
        width: 18%;
        margin-right: 2.5%; }
    .preview-thumbnail.nav-tabs li img {
        max-width: 100%;
        display: block; }
    .preview-thumbnail.nav-tabs li a {
        padding: 0;
        margin: 0; }
    .preview-thumbnail.nav-tabs li:last-of-type {
        margin-right: 0; }

    .tab-content {
        overflow: hidden; }
    .tab-content img {
        width: 100%;
        -webkit-animation-name: opacity;
        animation-name: opacity;
        -webkit-animation-duration: .3s;
        animation-duration: .3s; }

    .card {
        margin-top: 50px;
        background: #eee;
        padding: 3em;
        line-height: 1.5em; }

    @media screen and (min-width: 997px) {
        .wrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex; } }

    .details {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        margin-top: 10px; }

    .colors {
        -webkit-box-flex: 1;
        -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
        flex-grow: 1; }

    .product-title, .price, .sizes, .colors {
        text-transform: UPPERCASE;
        font-weight: bold; }

    .checked, .price span {
        color: #ff9f1a; }

    .product-title, .rating, .product-description, .price, .vote, .sizes {
        margin-bottom: 15px; }

    .product-title {
        margin-top: 0; }

    .size {
        margin-right: 10px; }
    .size:first-of-type {
        margin-left: 40px; }

    .color {
        display: inline-block;
        vertical-align: middle;
        margin-right: 10px;
        height: 2em;
        width: 2em;
        border-radius: 2px; }
    .color:first-of-type {
        margin-left: 20px; }

    .add-to-cart, .like {
        background: #ff9f1a;
        padding: 1.2em 1.5em;
        border: none;
        text-transform: UPPERCASE;
        font-weight: bold;
        color: #fff;
        -webkit-transition: background .3s ease;
        transition: background .3s ease; }
    .add-to-cart:hover, .like:hover {
        background: #b36800;
        color: #fff; }

    .not-available {
        text-align: center;
        line-height: 2em; }
    .not-available:before {
        font-family: fontawesome;
        content: "\f00d";
        color: #fff; }

    .orange {
        background: #ff9f1a; }

    .green {
        background: #85ad00; }

    .blue {
        background: #0076ad; }

    .tooltip-inner {
        padding: 1.3em; }

    @-webkit-keyframes opacity {
        0% {
            opacity: 0;
            -webkit-transform: scale(3);
            transform: scale(3); }
        100% {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1); } }

    @keyframes opacity {
        0% {
            opacity: 0;
            -webkit-transform: scale(3);
            transform: scale(3); }
        100% {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1); } }
</style>
<div class="modal-header bg-primary">
    <h5 class="modal-title">Complaint : {{ isset($get->title)?$get->title :'' }}</h5>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="modal-body clearfix">

        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="wrapper row">
                            <div class="preview col-md-6">

                                <div class="preview-pic tab-content">
                                    <div class="tab-pane active" id="pic-1">
                                        <img src="{{ URL::to('/') }}/uploads/images/{{$get->image}}"/>
                                    </div>
                                </div>
                                <!-- <ul class="preview-thumbnail nav nav-tabs">
                                    <li class="active"><a data-target="#pic-1" data-toggle="tab">
                                            <img src="{{ URL::to('/') }}/uploads/images/{{$get->image}}"/>
                                        </a></li>
                                </ul> -->

                            </div>
                            <div class="details col-md-6">
                                <h3 class="product-title">{{ $get->device->name  }}</h3>
                                <h3 class="product-title">{{ $get->name  }}</h3>

                                <p>
                                    Title: <b> {{ $get->title }} </b>  <br>
                                    Device: <b> {{ $get->device->name }} </b>  <br>

                                    Responded At: <b> {{ $get->responded_at->toFormattedDateString() }}</b>  <br>
                                    Assigned To: <b> {{$get->attend->name}} </b>  <br>
                                    Closed By: <b> {{ $get->close->name}} </b>  <br>
                                    Closed At: <b> {{$get->closed_at->toFormattedDateString()}} </b>  <br>
                                </p>
                                <h4 class="price">Cost: <span>{{ $get->cost }} </span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="details col-md-12 col-lg-12">
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseDetails" aria-expanded="false" aria-controls="collapseDetails">
                            More Details
                        </button>

                        <div class="collapse" id="collapseDetails">
                            <table class="table table-sm table-sm  table-striped table-hover table-bordered" width="100%">
                                <tr>
                                    <td style="width: 25%">Severity</td>
                                    <td style="width: 25%"><?php if($get->severity == 0){
                                            echo 'Minor';
                                        }elseif($get->severity == 1){
                                            echo 'Major';
                                        }
                                        elseif($get->severity == 2){
                                            echo 'Critical';
                                        }
                                     ?></td>
                                    <td style="width: 25%">Cost</td>
                                    <td style="width: 25%">{{ $get->cost }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 25%">Expected Completion Time</td>
                                    <td style="width: 25%">{{ $get->expected_completion_time }}</td>
                                    <td style="width: 25%">Reason for Pending</td>
                                    <td style="width: 25%">{{ $get->reason_for_pending }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 25%">More Info</td>
                                    <td style="width: 25%">{{ $get->more_info }}</td>
                                    <td style="width: 25%">Reason for Reassign</td>
                                    <td style="width: 25%">{{ $get->reason_for_reassign }} </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br>
                    @if(isset($get->complaints) && count($get->complaints) > 0)
                    <div class="details col-md-12 col-lg-12">
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            View Complaint Histories
                        </button>
                        <div class="collapse" id="collapseExample">
                            <table class="table table-sm table-sm  table-striped table-hover table-bordered" width="100%">
                                @foreach($get->complaints as $complaint)
                                <tr>
                                    <td>{{ $complaint->title }}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-backdrop="false" data-dismiss="modal">Close</button>
</div>
