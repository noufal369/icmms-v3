@extends('layouts.app')

@section('content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Devices</li>
      </ol>
      <div class="row">
        <div class="col-12">
          <h1>Device List</h1>
            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                i CMMS
                                <small>Machines</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li class="active">
                                    <i class="fa fa-gear"></i> Machine Details
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <i class="fa fa-info-circle"></i>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <p>
                                <button type="button" class="btn btn-primary" onclick="goBack()">Go Back</button>
                                <a href="" title="Edit" onclick="return confirm('Are you sure?');">
                                    <button type="button" class="btn btn-warning btn-sm">Edit  <span class="glyphicon glyphicon-edit"></span></button></a>


                            </p>
                        </div>

                        <div class="col-lg-7">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">

                                        <div class="col-lg-7 text-left">
                                            <div class="huge"><?php echo $get->name ?>
                                            </div>
                                            <div style="font-size: 14px;">
                                                Unique ID : <?php echo '<b>'.$get->code.'</b>'.'<br>'; ?>
                                                Department : <?php echo '<b>'. $get->department->name.'</b>'.'<br>'; ?>
                                                Serial No. : <?php echo '<b>'. $get->serial_no.'</b>'.'<br>'; ?>
                                                Model No. : <?php echo '<b>'. $get->model_no.'</b>'.'<br>'; ?>
                                                Supplier : <?php echo '<b>'. $get->supplier.'</b>'.'<br>'; ?>
                                                GMDN : <?php echo '<b>'. $get->gmdn.'</b>'.'<br>'; ?>
                                                Manufacturer : <?php echo '<b>'. $get->manufacturer.'</b>'.'<br>'; ?>
                                                Service Provider : <?php echo '<b>'. $get->service_provider.'</b>'.'<br>'; ?>
                                                Warranty Period : <?php echo '<b>'. $get->warranty_period.'</b>'.'<br>'; ?>
                                                Installation Date : <?php echo '<b>'. $get->install_date.'</b>'.'<br>'; ?>
                                                AMC Date : <?php echo '<b>'. $get->amc_date.'</b>'.'<br>'; ?>
                                                CMC Date : <?php echo '<b>'. $get->cmc_date.'</b>'.'<br>'; ?>
                                                PM Date : <?php echo '<b>'. $get->pm_date.'</b>'.'<br>'; ?>
                                                Purchase Cost : <?php echo '<b>'. $get->purchase_cost.'</b>'.'<br>'; ?>



                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <?php if(!empty($get->qr_code)){?>
                                            <img src="" class="img-thumbnail" />
                                            <?php } else {     echo '<span class="text-danger">'.'QR Code Not Uploaded'.'</span>'; } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if(!empty($get->description_pdf)){?>
                                <a href="" target="_blank">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Description PDF File</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                                <?php } else { ?>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">No PDF File</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                                <?php  }?>
                            </div>
                        </div>

                        <div class="col-lg-5">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Photo</h3>
                                </div>
                                <div class="panel-body">
                                    <?php if(!empty($get->photo)){?>
                                    <img src="" class="img-thumbnail" />
                                    <?php } else {     echo '<span class="text-danger">'.'Photo Not Uploaded'.'</span>'; } ?>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <h2 class="page-header"><?php echo $get->name; ?> <small>Specifications</small></h2>
                                <table class="table table-sm table-bordered table-hover table-striped">
                                    <thead>
                                    <tr  class="info"><th>Purchase Date</th><td><?php echo ($get->purchase_date); ?>

                                        </td><th>Spare Parts</th>
                                        <td>

                                        </td>
                                    </tr>
                                    <tr class="active"><th>Purchase Order No</th><td><?php echo $get->purchase_order_no; ?></td><th>Accessories</th>
                                        <td> </td>
                                    </tr>
                                    <tr class="info"><th>Calibration Frequency</th><td><?php echo $get->calibration_frequency; ?></td>
                                        <th>PM Frequency</th><td><?php echo $get->pm_frequency; ?></td></tr>
                                    <tr class="active"><th>Firmware Version</th><td><?php echo $get->firmware_version; ?></td><th>Ownership</th><td><?php echo $get->ownership; ?></td></tr>
                                    <tr  class="info"><th>Inspection Details</th><td><?php echo $get->inspection_details; ?></td><th>Calibration Date</th><td><?php echo ($get->calibration_date); ?></td></tr>
                                    <tr class="active"><th>Repair Histories</th><td><a href="machines/report/<?php echo $get->machine_id;?>">View</a></td><th>Risk Classification</th><td><?php echo $get->risk_classification; ?></td></tr>
                                    <tr  class="info"><th>Applicable Device</th><td><?php echo $get->applicable_device; ?></td><th>Adverse Events</th><td><?php echo $get->adverse_events; ?></td></tr>
                                    <tr class="active"><th>Other Information</th><td><?php echo $get->other_info; ?></td><th>Comments</th><td><?php echo $get->comments; ?></td></tr>
                                    <tr  class="info"><th>Machine Status</th><td><?php echo $get->machine_status; ?></td><th>Power Spec</th><td><?php echo $get->power_spec; ?></td></tr>
                                    <tr  class="active"><th>Created Date</th><td><span class="text-danger"><?php echo ($get->create_date); ?></span></td>
                                        <th>Updated Date</th><td><span class="text-danger"><?php echo ($get->update_date); ?></span></td></tr>
                                    </thead>

                                </table>
                            </div>

                        </div>
                    </div>
                <!-- /.row -->

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
  </div>

@endsection
