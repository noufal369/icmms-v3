@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Tutorials</li>
            </ol>
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <h1>Tutorials
                        <small>
                            <button type="button" class="btn btn-primary pull-right" onclick="viewCreate()">Add +
                            </button>
                        </small>
                    </h1>
                    <table class="table table-sm table-hover table-bordered datatable"  width="100%" cellspacing="0">
                        <thead class="thead-dark">
                        <tr>
                            <th>Device</th>
                            <th>Search Tags</th>
                            <th>Description</th>
                            <th>By User</th>
                            <th>Updated</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot class="thead-dark">
                        <tr>
                            <th>Device</th>
                            <th>Search Tags</th>
                            <th>Description</th>
                            <th>By User</th>
                            <th>Updated</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
    </div>

@endsection
@section('footer_scripts')
    <script>

        // DataTable
        $(document).ready(function () {
            $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('tutorials.data') }}',
                columns: [
                    {data: 'device', name: 'device'},
                    {data: 'search_tags', name: 'search_tags'},
                    {data: 'description', name: 'description'},
                    {data: 'by_user', name: 'by_user'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false}
                ]
            });
        });


        // View Tutorial
        $(document).on('click', '[data-view]', function () {
            var id = $(this).attr('data-view');
            if (Number($(this).attr('data-view')) > 0) {
                viewTutorial(id);
            }
        });
        var device;

        function viewTutorial(id) {
            if (typeof(id) !== "undefined") {
                if (id > 0) {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        type: 'GET',
                        url: "tutorials/" + id,
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            $('#showData').html(response.data);
                            $('.showModal').modal('show');
                        }
                    });
                }
            }
        }

        // View Create Page

        function viewCreate() {
            if (typeof device !== 'undefined')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "tutorials/create/",
                dataType: 'JSON',
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {
                    $('#showData').html(response.data);
                    $('.showModal').modal('show');
                }
            });
        }

        // View Edit Page

        function viewEdit(id) {
            if (typeof device !== 'undefined')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "tutorials/" + id + "/edit",
                dataType: 'JSON',
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {
                    $('#showData').html(response.data);
                    $('.showModal').modal('show');
                }
            });
        }

        // Delete Tutorial

        $(document).on('click', '[data-delete]', function () {
            var ele = $(this);
            var id = $(this).attr('data-delete');
            if (Number($(this).attr('data-delete')) > 0) {
                deleteTutorial(id, ele);
            }
        });

        // Toastr             toastr["error"](" cannot be Empty", "");

        function deleteTutorial(id, ele) {
            var msgTxt;
            var msgTitle;
            if (ele.find('i').hasClass('fa-recycle')) {
                msgTxt = "Tutorial Activated";
                msgTitle = "Are you sure you want to activate ";
            } else {
                msgTxt = "Tutorial Deleted";
                msgTitle = "Are you sure you want to delete ";
            }
            swal({
                title: msgTitle,
                text: "This device will be disabled and removed from listing.",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                setTimeout(function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        type: 'DELETE',
                        url: "tutorials/" + id,
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            if (response.status === 201) {
                                if (ele.find('i').hasClass('fa-recycle')) {
                                    ele.find('i').attr('class', 'fa fa-trash-o').attr('title', 'Delete Tutorial');
                                    swal({title: "Tutorial deletion failed", type: "success"});
                                } else {
                                    ele.find('i').attr('class', 'fa fa-recycle').attr('title', 'Activate Tutorial');
                                    swal({title: "Tutorial deletion failed", type: "success"});
                                }
                                swal({title: msgTxt, type: "success"});
                            } else {
                                swal({title: "Tutorial deletion failed", type: "error"});
                            }
                        }
                    });
                }, 1000);
            });
        }
    </script>
@endsection
