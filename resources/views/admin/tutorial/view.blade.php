<div class="modal-header bg-primary">
    <h5 class="modal-title">View Tutorial Details</h5>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="modal-body clearfix">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-12">
                            <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                <h4>Basic Information</h4>
                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label for="name" class="control-label">Name*</label>
                                        <input type="text" id="name" name="name" class="form-control"
                                               value="{{$get->name}}"
                                               max="50" disabled>
                                    </div>
                                    <div class="col">
                                        <label for="location" class="control-label">Location</label>
                                        <input type="text" id="location" name="location" class="form-control"
                                               value="{{$get->location}}" max="50" disabled>
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label for="email" class="control-label">Email</label>
                                        <input type="email" id="email" name="email" class="form-control"
                                               value="{{$get->email}}"
                                               max="190" disabled>
                                    </div>
                                    <div class="col">
                                        <label for="phone" class="control-label">Phone</label>
                                        <input type="tel" id="phone" name="phone" class="form-control"
                                               value="{{$get->phone}}"
                                               max="190" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-backdrop="false" data-dismiss="modal">Close</button>
</div>
