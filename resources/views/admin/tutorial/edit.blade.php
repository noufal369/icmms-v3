<div class="modal-header bg-primary">
    <h5 class="modal-title">Edit a Tutorial</h5>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="modal-body clearfix">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-12">
                            <form class="form-group company-form" role="form" method="POST"
                                  action="{{ route('tutorials.update', $get->id) }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                    <h4>Basic Information</h4>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="device" class="control-label">Device*</label>
                                            <input type="text" id="device" name="device" class="form-control"
                                                   value="{{$get->device}}" max="50">
                                        </div>
                                        <div class="col">
                                            <label for="tags" class="control-label">Location</label>
                                            <input type="text" id="tags" name="tags" class="form-control"
                                                   value="{{$get->tags}}" max="50">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="description" class="control-label">Description</label>
                                            <textarea class="form-control" rows="3" name="description"
                                                      placeholder="Description" maxlength="300">{{$get->description}}</textarea>
                                        </div>
                                        <div class="col">
                                            <label for="exampleInputFile">Document</label>
                                            <input type="file" name="document" class="form-control-file"
                                                   id="exampleInputFile" aria-describedby="fileHelp">
                                            <small id="fileHelp" class="form-text text-muted">Select a file to use as the
                                                fullscreen ad image. Please ensure the size is at least 1080x1920 with a
                                                9:16 (portrait) aspect ratio.
                                            </small>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block" id="activate">Update
                                        Tutorial
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-backdrop="false" data-dismiss="modal">Close</button>
</div>
