<div class="modal-header bg-info">
    <h4 class="title" id="largeModalLabel" style="color: #fff;">Edit Department</h4>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
                        <div class="col-12">
                            <form class="form-group company-form" role="form" method="POST"
                                  action="{{ route('departments.update', $get->id) }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="name" class="control-label">Name*</label>
                                            <input type="text" id="name" name="name" class="form-control"
                                                   value="{{$get->name}}" max="50">
                                        </div>
                                        <div class="col">
                                            <label for="company" class="control-label">Location</label>
                                            <select class="form-control" name="company_id" id="company">
                                                <option value="" selected>Select Company</option>
                                                @foreach ($companies as $value)
                                                    <option value="{{ $value->id }}" {{ $value->id == $get->company_id ? 'selected="selected"' : ''}}>
                                                        {{ $value->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round btn-block" id="activate">Update
                                        Department
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>    
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
</div>