<div class="modal-header bg-info">
    <h4 class="title" id="largeModalLabel" style="color: #fff">Spare Part Informations</h4>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
         <div class="col-12">
                            <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="name" class="control-label">Name*</label>
                                            <input type="text" id="name" name="name" class="form-control"
                                                   value="{{$get->name}}" max="50" disabled="disabled">
                                        </div>
                                        <div class="col">
                                            <label for="model_no" class="control-label">Model No</label>
                                            <input type="text" id="model_no" name="model_no" class="form-control"
                                                   value="{{$get->model_no}}" max="50" disabled="disabled">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label class="control-label">Warranty Date</label>
                                            <input type="date" name="warranty_date" class="form-control"
                                                   placeholder="Warranty Date eg: 2016-12-31" value="{{$get->warranty_date}}" disabled="disabled">
                                        </div>
                                        <div class="col">
                                            <label for="serial_no" class="control-label">Serial No</label>                                            
                                            <input type="text" id="serial_no" name="serial_no" class="form-control"
                                                   placeholder="Serial No" max="190" required="required" value="{{$get->serial_no}}" disabled="disabled">
                                        </div>    
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="manufacturer" class="control-label">Manufacturer</label>
                                            <input type="text" id="manufacturer" name="manufacturer" class="form-control"
                                                   placeholder="Manufacturer" max="190" value="{{$get->manufacturer}}" disabled="disabled">
                                        </div>
                                        <div class="col">
                                            <label for="stock" class="control-label">Stock</label>
                                            <input type="text" id="stock" name="stock" class="form-control"
                                                   placeholder="Stock" max="190" value="{{$get->stock}}" disabled="disabled">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12"> 
                                        <div class="col">
                                            <label for="stock" class="control-label">SKU</label>
                                            <input type="text" id="sku" name="sku" class="form-control"
                                                   placeholder="SKU" max="190" value="{{$get->sku}}" disabled="disabled">
                                        </div>
                                        <div class="col">
                                            <label class="control-label">AMC Date</label>
                                            <input type="date" name="date" class="form-control"
                                                   placeholder="AMC Date eg: 2016-12-31" value="{{$get->date}}" disabled="disabled">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="cost" class="control-label">Cost</label>
                                            <input type="text" id="cost" name="cost" class="form-control"
                                                   placeholder="Cost" max="190" value="{{$get->cost}}" disabled="disabled">
                                        </div>
                                        <div class="col">
                                            <label for="comments" class="control-label">Comments</label>
                                            <input type="text" id="other_info" name="other_info" class="form-control"
                                                   placeholder="Comments" max="190" value="{{$get->other_info}}" disabled="disabled">
                                        </div>
                                    </div>
                                    @if($get->image)
                                    <div class="form-group form-row col-sm-12">
                                        <img class="text-center" src="{{ URL::to('/') }}/uploads/images/{{$get->image}}" style="width: 650px;height: 300px;" />
                                    </div>
                                    @endif
                            </div>
                        </div>               
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
</div>