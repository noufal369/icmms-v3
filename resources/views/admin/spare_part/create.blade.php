<style type="text/css">
    .remove_field{
        height: 35px;
        margin-top: 36px;
        margin-right: 5px;
        margin-left: -5px;
    }
    .add-serial{
        height: 35px;
        margin-top: 0px;
    }
</style>

<div class="modal-header bg-info">
    <h4 class="title" id="largeModalLabel" style="color: #fff;">Add Spare Part</h4>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
    <div class="modal-body">
        <div class="row">
                <div class="col-12">
                    <form class="form-group company-form" role="form" method="post"
                                  action="{{ route('spare_parts.store') }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="name" class="control-label">Name*</label>
                                            <input type="text" id="name" name="name" class="form-control"
                                                   placeholder="Name" max="50">
                                        </div>
                                        <div class="col">
                                            <label for="model_no" class="control-label">Model No</label>
                                            <input type="text" id="model_no" name="model_no" class="form-control"
                                                   placeholder="Model No" max="50">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label class="control-label">Warranty Date</label>
                                            <input type="date" name="warranty_date[]" class="form-control"
                                                   placeholder="Warranty Date eg: 2016-12-31">
                                        </div>
                                        <div class="col">
                                            <label for="serial_no" class="control-label">Serial No</label>
                                            <div class="input-group">
                                            <input type="text" id="serial_no" name="serial_no[]" class="form-control"
                                                   placeholder="Serial No" max="190" required="required">
                                                <div class="input-group-btn"><button type="button" class="btn btn-success add-serial" id="add_serial"><i class="fa fa-plus-square"></i></button>  
                                                </div>
                                            </div>
                                        </div>    
                                    </div>

                                    <div id="addSerial"></div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="manufacturer" class="control-label">Manufacturer</label>
                                            <input type="text" id="manufacturer" name="manufacturer" class="form-control"
                                                   placeholder="Manufacturer" max="190">
                                        </div>
                                        <div class="col">
                                            <label for="stock" class="control-label">Stock</label>
                                            <input type="text" id="stock" name="stock" class="form-control"
                                                   placeholder="Stock" max="190">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12"> 
                                        <div class="col">
                                            <label for="stock" class="control-label">SKU</label>
                                            <input type="text" id="sku" name="sku" class="form-control"
                                                   placeholder="SKU" max="190">
                                        </div>
                                        <div class="col">
                                            <label class="control-label">AMC Date</label>
                                            <input type="date" name="date" class="form-control"
                                                   placeholder="AMC Date eg: 2016-12-31">
                                        </div>
                                    </div>
                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="cost" class="control-label">Cost</label>
                                            <input type="text" id="cost" name="cost" class="form-control"
                                                   placeholder="Cost" max="190">
                                        </div>
                                        <div class="col">
                                            <label for="comments" class="control-label">Image</label>
                                            <input type="file" id="image" name="image" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="comments" class="control-label">Comments</label>
                                            <input type="text" id="other_info" name="other_info" class="form-control"
                                                   placeholder="Comments" max="190">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round btn-block" id="activate">Add
                                        Spare Part
                                    </button>
                                </div>
                            </form>
            </div>
        </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
</div>



<script>
 
        $(document).ready(function() {
        var max_fields      = 6; //maximum input boxes allowed
        var wrapper         = $("#addSerial"); //Fields wrapper
        var add_button      = $("#add_serial"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="form-group form-row col-sm-12"><div class="col"><label class="control-label">Warranty Date</label><input type="date" name="warranty_date[]" class="form-control" placeholder="Warranty Date eg: 2016-12-31"></div><div class="col"><label for="serial_no" class="control-label">Serial No</label><div class="input-group"><input type="text" id="serial_no" name="serial_no[]" class="form-control" placeholder="Serial No" max="190" required="required"><div class="input-group-btn"></div></div></div><a href="javascript:void(0)" class="btn btn-danger remove_field"><span class="fa fa-minus-square"></span></a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
    });

    </script>
