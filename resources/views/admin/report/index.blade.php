@extends('layouts.app')

@section('content')
    <style>
        .input-group-addon
        {
            background-color: rgb(50, 118, 177);
            border-color: rgb(40, 94, 142);
            color: rgb(255, 255, 255);
        }
        .form-signup input[type="text"],.form-signup input[type="password"] { border: 1px solid rgb(50, 118, 177); }

    </style>

    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Reports</li>
            </ol>
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <h1>Reports</h1>

                    <div id="fullscreen_bg" class="fullscreen_bg">
                        <form class="form-signin">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-md-offset-2">

                                        <div class="panel panel-default">

                                            <div class="panel panel-primary">


                                                <div class="text-center">
                                                    <h3 style="color:#2C3E50">Financial Reports</h3>
                                                    <h4><label for="Choose Report" style="color:#E74C3C">Choose
                                                            Report</label></h4>
                                                    <div class="input-group"><span class="input-group-addon"><span
                                                                    class="glyphicon glyphicon-tasks"></span></span>
                                                        <select class="form-control">
                                                            <option value="Income" selected>Complaints</option>
                                                            <option value="Expenses">PMs</option>
                                                            <option value="Expenses">Total Expenses</option>
                                                            <option value="Profit">Profit</option>
                                                        </select></div>
                                                    <br>
                                                    <h5><label for="Choose Report" style="color:#E74C3C"> Time :</label>
                                                        <input id="a" type="radio" name="type" value="Daily">Daily
                                                        <input id="b" type="radio" name="type" value="Weekly">Weekly
                                                        <input id="c" type="radio" name="type" value="Monthly">Monthly
                                                    </h5>

                                                    <div class="customer">
                                                        <div class="input-group">
                                                        <span class="input-group-addon"><span
                                                                    class="glyphicon glyphicon-calendar"></span></span>
                                                            <input type="date" class="form-control" placeholder="Date"/>
                                                        </div>
                                                    </div>
                                                    </br>
                                                    <button type="button" class="btn btn-primary btn-lg btn3d"><span
                                                                class="glyphicon glyphicon-search"></span> View
                                                    </button>
                                                    <br>
                                                </div>
                                                <div class="panel-body" style="margin-top: 20px">

                                                    <table class="table table-sm table-striped table-condensed" id="example">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center" width="115px">Work Order No</th>
                                                            <th class="text-center" width="115px">Engineer</th>
                                                            <th class="text-center" width="115px">Hours Spent</th>
                                                            <th class="text-center" width="115px">Date</th>
                                                            <th class="text-center" width="115px">More Details</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>

                                                            <td class="text-center" width="150px">1</td>
                                                            <td class="text-center" width="150px">Mai Ahmed</td>
                                                            <td class="text-center" width="150px">500</span></td>
                                                            <td class="text-center" width="150px">3/12/2016</span></td>
                                                            <td class="text-center" width="150px">
                                                                Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">2</td>
                                                            <td class="text-center">Mai Ahmed</td>
                                                            <td class="text-center">60</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Appoitment</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Ahmed Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        <tr>

                                                            <td class="text-center">3</td>
                                                            <td class="text-center">Waled</td>
                                                            <td class="text-center">180</span></td>
                                                            <td class="text-center">3/12/2016</span></td>
                                                            <td class="text-center">Prescription</span></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="text-center">
                                                        <h4><label style="color:#E74C3C" for="Total">Total DownTime :</label>7740 Hrs
                                                        </h4></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
    </div>

@endsection
@section('footer_scripts')
    <script>

        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            } );
        } );

        $(document).ready(function(){

            $(".customer").toggle();

        });

        $(document).ready(function(){
            $("#a").click(function(){
                $(".customer").show();
            });
        });

        $(document).ready(function(){
            $("#b").click(function(){
                $(".customer").hide();
            });
        });

        $(document).ready(function(){
            $("#c").click(function(){
                $(".customer").hide();
            });
        });


    </script>
@endsection
