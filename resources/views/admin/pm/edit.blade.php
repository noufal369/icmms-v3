@extends('layouts.app')
@section('title', 'Edit PM')
@section('page-style')
@section('content')

<link rel="stylesheet" href="{{asset('assets/plugins/multi-select/css/multi-select.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.css')}}"/>

<script src="{{asset('assets/plugins/multi-select/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/plugins/quick-search/js/jquery.quicksearch.js')}}"></script>

<style>
.progress .progress-bar{
        height: 15px;
    }
.custom-header {
    text-align: center;
    padding: 3px;
    background: #1cbfd0;
    color: #fff;
    font-size: 15px;
    height: 35px;
    }
.sweet-alert h2 {
    margin: 30px 0px 5px 0px !important;
}
.my-data div{
    float:left;
    width:50%;
    /*text-align:center;*/
    position:relative;
    padding-bottom: 10px;
}

</style>
<div class="row clearfix">

        <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="product details">
                            <h3 class="product-title mb-0">{{$get->device->name}}</h3><br>
                            <h5 class="price mt-0">Device Code : <span class="col-purple">{{$get->device->code}}</span></h5>                                
                            <h5 class="price mt-0">PM Order No : <span class="col-purple">{{ $get->order_no }}</span></h5>
                            
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-sm  {{ 'pending' == $get->progress ? 'btn-danger' : 'btn-secondary'}} pending" onclick="changeProgress('pending')">
                                    <input type="radio" name="progress" id="option1" value="pending" 
                                        autocomplete="off" {{ 'pending' == $get->progress ? 'checked' : ''}}>
                                        PENDING
                                </label>
                                <label class="btn  btn-sm  {{ 'ongoing' == $get->progress ? 'btn-warning' : 'btn-secondary'}} in-progress" onclick="changeProgress('ongoing')">
                                    <input type="radio" name="progress" id="option2" value="ongoing" 
                                        autocomplete="off" {{ 'ongoing' == $get->progress ? 'checked' : ''}}>
                                        IN PROGRESS
                                </label>
                                <label class="btn  btn-sm {{ 'completed' == $get->progress ? 'btn-success' : 'btn-secondary'}} completed" onclick="changeProgress('completed')">
                                    <input type="radio" name="progress" id="option3" value="completed" 
                                        autocomplete="off" {{ 'completed' == $get->progress ? 'checked' : ''}}>
                                        COMPLETED
                                </label>
                            </div>

                                            
                                <div class="btn-group" id="btn-group" style="float: right;">                                                    
                                    <div id="start-btn">
                                    @if(empty($log))
                                    <button type="button" class="btn btn-success" id="start" onclick="time_log({{$get->id}}, 1)"><i class="fa fa-hourglass-start"></i><span style="margin-left: 10px;">START</span></button>
                                    @endif
                                    </div>                                                    
                                    <div id="pause-btn">
                                    @if(!empty($log))
                                    @if($log[0]['status'] == 1 || $log[0]['status'] == 3)
                                    <button type="button" class="btn btn-warning" id="pause" onclick="time_log({{$get->id}}, 2)"><i class="fa fa-pause"></i><span style="margin-left: 10px;">PAUSE</span></button>
                                    @endif
                                    @endif
                                    </div>
                                                    
                                    <div id="resume-btn">
                                    @if(!empty($log))
                                    @if($log[0]['status'] == 2)
                                    <button type="button" class="btn btn-info" id="resume" onclick="time_log({{$get->id}}, 3)"><i class="fa fa-play"></i><span style="margin-left: 10px;">RESUME</span></button>
                                    @endif
                                    @endif
                                    </div>

                                    <div id="stop-btn">
                                    @if(!empty($log) && $log[0]['status'] != 4)
                                    <button type="button" class="btn btn-danger" id="stop" onclick="time_log({{$get->id}}, 4)"><i class="fa fa-stop-circle"></i><span style="margin-left: 10px;">STOP</span></button>
                                    @endif
                                    </div>
                                                    
                                    </div>                                             


                            <div id="alert-div" style="margin-top: 20px;">
                            @if(!empty($log)) 
                            @if($log[0]['status'] != 1 && $log[0]['status'] != 3)
                            <div class="alert alert-info">
                                <strong>User : </strong>
                                                    
                                {{$userArray[$get->assigned_to]}}
                                                    
                                <br>
                                <strong>Total Time Worked : </strong>
                                <?php
                                if($log[0]['log_time']  < 60){
                                    echo $log[0]['log_time']. ' Minutes';
                                }
                                else{
                                    $hours = (int)($log[0]['log_time']/60);
                                    $minutes = $log[0]['log_time'] % 60;

                                    echo $hours. ' Hour '.$minutes.' Minutes';
                                }
                                ?>
                            </div>
                            @endif 
                            @endif
                            </div>  

                            <hr>
                            <p class="product-description">
                                <div class="my-data">
                                    <div class="area">
                                        <strong>Serial No : </strong>
                                        {{$get->device->serial_no}}
                                    </div>
                                    <div class="area">
                                        <strong>Manufacturer : </strong>
                                        {{ isset($get->manufacturer->name)?$get->manufacturer->name : '' }}
                                        <span></span>
                                    </div>
                                    <div class="area">
                                        <strong>Assigned To : </strong>
                                        {{$userArray[$get->assigned_to]}}
                                    </div>
                                    <div class="area">
                                        <strong>Activity Done : </strong>
                                        {{$get->title}}
                                    </div>
                                    <div class="area">
                                        <strong>Spare Parts : </strong>
                                        @if(!is_null($get->spare_part_id))
                                            @foreach($spares as $item)
                                               @if(in_array($item->id, json_decode($get->spare_part_id)))
                                                    {{$item->name}} , &nbsp;
                                               @endif
                                            @endforeach
                                        @endif
                                    </div>                                    
                                    <div class="area">
                                        <strong>Cost Involved : </strong>
                                        {{ $get->cost }}
                                    </div>
                                    <div class="area">
                                        <strong>Detailed Activity : </strong>
                                            {{ $get->report }}
                                    </div>                                    
                                    <div class="area">
                                        <strong>Remarks : </strong>
                                        {{ $get->remarks }}
                                    </div>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item basic">
                                    <a class="nav-link active" data-toggle="tab" href="#infoPanel" role="tab">Basic
                                        Information</a>
                                <li>
                                <li class="nav-item more">
                                    <a class="nav-link" data-toggle="tab" href="#ads" role="tab">More Details</a>
                                <li>
                                <li class="nav-item options">
                                    <a class="nav-link" data-toggle="tab" href="#options" role="tab">Options</a>
                                <li>
                                <li class="nav-item attach">
                                    <a class="nav-link" data-toggle="tab" href="#placementPanel"
                                       role="tab">Attachments</a>
                                <li>
                            </ul><hr>

                            <form class="form-group company-form" role="form" method="POST" action="{{ route('pm.update', $get->id) }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <div class="tab-content mt-2">

                                    <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                        <br>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Assigned To</label>
                                                <select class="form-control" name="assigned_to">
                                                <option value="">-Select-</option>
                                                @foreach ($users as $value)
                                                <option value="{{ $value->id }}" @if($value->id == $get->assigned_to) selected="selected" @endif>{{ $value->name }} ({{ $value->email }})</option>
                                            @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group form-row col-sm-12" style="margin-top: 15px;">
                                                <div class="col">
                                                    <label class="control-label">Activity Done</label>
                                                    <input type="text" name="title"
                                                           class="form-control" placeholder="Activity Done" value="{{ $get->title }}" 
                                                        >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Detailed Activity</label>
                                                <textarea name="report" class="form-control" rows="3" placeholder="Detailed Activity">{{ $get->report }}</textarea>
                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-secondary pull-right" id="infoContinue">Continue</button>
                                    </div>

                                    <div class="tab-pane fade" id="ads" role="tabpanel">
                                        <?php $list = json_decode($device['checklist']); ?>
                                        @if(!empty($list))
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Checklist</label>
                                                @foreach ($list as $value)
                                                    <div class="checkbox">
                                                        <input id="checklist{{$value}}" type="checkbox" value="{{$value}}" name="pm_checklist[]" 
                                                        <?php if(!is_null($get->pm_checklist)) { if(in_array($value, json_decode($get->pm_checklist))) { ?> checked="checked"  <?php } } ?>>
                                                        <label for="checklist{{$value}}">
                                                            {{$check[$value] }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Cost Involved</label>
                                                <input type="text" name="cost"
                                                       class="form-control" placeholder="Cost"
                                                       max="50" value="{{ $get->cost }}">
                                            </div>
                                        </div>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Remarks</label>
                                                <textarea name="remarks" class="form-control" rows="3" placeholder="Remarks">{{ $get->remarks }}</textarea>
                                            </div>
                                        </div>
                                        <br>
                                        <button class="btn btn-secondary pull-right" id="adsContinue">Continue</button>

                                    </div>

                                    <div class="tab-pane fade" id="options" role="tabpanel">
                                        <div class="form-group form-row col-sm-12" style="margin-top: 20px;">
                                            <div class="col">
                                                <label for="control-label">Spare Parts Used</label>
                                                <select name="spare_part_id[]" id="spare_part_id" class="form-control ms" multiple="multiple">
                                                    @foreach($spares as $item)
                                                    <option value="{{$item['id']}}" <?php if(!is_null($get->spare_part_id)) { if(in_array($item->id, json_decode($get->spare_part_id))) { ?> selected="selected"  <?php } } ?>>{{$item['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>  
                                        </div>

                                        <br>
                                        <button class="btn btn-secondary pull-right" id="optContinue">Continue</button>
                                    </div>

                                    <div class="tab-pane fade" id="placementPanel" role="tabpanel">
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label for="exampleInputFile">Document</label>
                                                <input type="file" name="document" class="form-control-file"
                                                       id="exampleInputFile" aria-describedby="fileHelp">
                                                <small id="fileHelp" class="form-text text-muted">
                                                    Select a file to use as the fullscreen ad image. Please ensure the
                                                    size is at least 1080x1920 with a 9:16 (portrait) aspect ratio.
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="progress mt-5">
                                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="20"
                                     aria-valuemin="0" aria-valuemax="100">Step 1 of 4
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-round" id="activate">Update Report
                                </button>
                                <!-- <button type="button" class="btn btn-secondary" data-backdrop="false" data-dismiss="modal">Close</button> -->
                            </div>
                        </form>
                        </div>

                    </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('#infoContinue').click(function (e) {
            e.preventDefault();
            $('.progress-bar').css('width', '50%');
            $('.progress-bar').html('Step 2 of 4');
            $('#myTab a[href="#ads"]').tab('show');
        });

        $('#adsContinue').click(function (e) {
            e.preventDefault();
            $('.progress-bar').css('width', '75%');
            $('.progress-bar').html('Step 3 of 4');
            $('#myTab a[href="#options"]').tab('show');
        });

        $('#optContinue').click(function (e) {
            e.preventDefault();
            $('.progress-bar').css('width', '100%');
            $('.progress-bar').html('Step 4 of 4');
            $('#myTab a[href="#placementPanel"]').tab('show');
        });
    })

</script>

<script>
    $('.pending').click(function(){
        $('.pending').removeClass('btn-secondary');
        $('.pending').addClass('btn-danger');
        $('.in-progress').removeClass('btn-warning');
        $('.completed').removeClass('btn-success');
        $('.in-progress').addClass('btn-secondary');
        $('.completed').addClass('btn-secondary');
    });
    $('.in-progress').click(function(){
        $('.in-progress').removeClass('btn-secondary');
        $('.pending').removeClass('btn-danger');
        $('.in-progress').addClass('btn-warning');        
        $('.completed').removeClass('btn-success');
        $('.pending').addClass('btn-secondary');
        $('.completed').addClass('btn-secondary');
    });
    $('.completed').click(function(){
        $('.completed').removeClass('btn-secondary');
        $('.pending').removeClass('btn-danger');
        $('.in-progress').removeClass('btn-warning');
        $('.completed').addClass('btn-success');
        $('.pending').addClass('btn-secondary');
        $('.in-progress').addClass('btn-secondary');
    });
</script>

<script type="text/javascript">
        $('#spare_part_id').multiSelect({
            selectableHeader: "<div class='custom-header'>Available</div>",
            selectionHeader: "<div class='custom-header'>Selected</div>"
        });
</script>

<script>
    function changeProgress(progress)
    {
        var msgTxt;
        var msgTitle;
            
        msgTxt = "Done !!!";
        msgTitle = "Are you sure you want to continue ?";

        swal({
                title: msgTitle,
                text: "",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                setTimeout(function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        url: "{{ route('pms.progress', $get->id) }}",
                        data: { progress:progress },
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            if (response.status === 201) {
                                swal({title: msgTxt, type: "success"});
                            } else {
                                swal({title: "Process failed", type: "error"});
                            }
                        }
                    });
                }, 1000);
            });

    }
</script>

<script>
    function time_log(id, ele) {
            var msgTxt;
            var msgTitle;
            var device_id = {{ $get->device_id }};
            if(ele == 1){
                msgTxt = "Started !!!";
                msgTitle = "Are you sure you want to start ?";
            }else if(ele == 2){
                msgTxt = "Paused !!!";
                msgTitle = "Are you sure you want to pause ?";
            }else if(ele == 3){
                msgTxt = "Resumed !!!";
                msgTitle = "Are you sure you want to resume ?";
            }else if(ele == 4){
                msgTxt = "Stopped !!!";
                msgTitle = "Are you sure you want to stop ?";
            }
            
            
            swal({
                title: msgTitle,
                text: "",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                setTimeout(function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        url: "{{ route('pms.log', $get->id) }}",
                        data: { type:ele, device_id:device_id },
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            if (response.status === 201) {
                                if(ele == 1){
                                    $('#start-btn').empty();
                                    $('#resume-btn').empty();
                                    $('#pause-btn').empty();
                                    $('#stop-btn').empty();
                                    $('#start-btn').append('<button type="button" class="btn btn-warning" id="pause" onclick="time_log({{$get->id}}, 2)"><i class="fa fa-pause"></i><span style="margin-left: 10px;">PAUSE</span></button><button type="button" class="btn btn-danger" id="stop" onclick="time_log({{$get->id}}, 4)"><i class="fa fa-stop-circle"></i><span style="margin-left: 10px;">STOP</span></button>');
                                }
                                if(ele == 2){
                                    $('#start-btn').empty();
                                    $('#resume-btn').empty();
                                    $('#pause-btn').empty();
                                    $('#stop-btn').empty();
                                    $('#resume-btn').append('<button type="button" class="btn btn-info" id="resume" onclick="time_log({{$get->id}}, 3)"><i class="fa fa-play"></i><span style="margin-left: 10px;">RESUME</span></button><button type="button" class="btn btn-danger" id="stop" onclick="time_log({{$get->id}}, 4)"><i class="fa fa-stop-circle"></i><span style="margin-left: 10px;">STOP</span></button>');
                                    $('#alert-div').empty();
                                    $('#alert-div').append(response.data);
                                }
                                if(ele == 3){
                                    $('#start-btn').empty();
                                    $('#resume-btn').empty();
                                    $('#pause-btn').empty();
                                    $('#stop-btn').empty();
                                    $('#pause-btn').append('<button type="button" class="btn btn-warning" id="pause" onclick="time_log({{$get->id}}, 2)"><i class="fa fa-pause"></i><span style="margin-left: 10px;">PAUSE</span></button><button type="button" class="btn btn-danger" id="stop" onclick="time_log({{$get->id}}, 4)"><i class="fa fa-stop-circle"></i><span style="margin-left: 10px;">STOP</span></button>');
                                    $('#alert-div').empty();
                                }
                                if(ele == 4){
                                    $('#btn-group').remove();
                                    $('#alert-div').empty();
                                    $('#alert-div').append(response.data);
                                }
                                
                                swal({title: msgTxt, type: "success"});
                            } else {
                                swal({title: "Process failed", type: "error"});
                            }
                        }
                    });
                }, 1000);
            });
        }
</script>

@endsection
@section('footer_scripts')