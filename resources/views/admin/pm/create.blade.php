@extends('layouts.app')
@section('title', 'PM')
@section('page-style')
@section('content')

<link rel="stylesheet" href="{{asset('assets/plugins/multi-select/css/multi-select.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.css')}}"/>

<script src="{{asset('assets/plugins/multi-select/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/plugins/quick-search/js/jquery.quicksearch.js')}}"></script>

<style>
.progress .progress-bar{
        height: 15px;
    }
.custom-header {
    text-align: center;
    padding: 3px;
    background: #1cbfd0;
    color: #fff;
    font-size: 15px;
    height: 35px;
    }
.my-data div{
    float:left;
    width:50%;
    /*text-align:center;*/
    position:relative;
    padding-bottom: 10px;
}
</style>

<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="product details">
                            <h3 class="product-title mb-0">{{$get->name}}</h3><br>                                 
                            <h5 class="price mt-0">ID : <span class="col-purple">{{ $get->code }}</span></h5>
                            <h5 class="price mt-0">Risk Classification: 
                                @if($get->category_id == 1 )
                                    <span class="col-red">{{ $get->category->name }}</span>
                                @elseif($get->category_id == 2)
                                    <span class="col-amber">{{ $get->category->name }}</span>
                                @elseif($get->category_id == 3)
                                    <span class="col-green">{{ $get->category->name }}</span>
                                @endif
                            </h5>
                            <hr>
                            <p class="product-description">
                                <div class="my-data">
                                    <div class="area">
                                        <strong>Serial No : </strong>
                                        {{$get->serial_no}}
                                    </div>
                                    <div class="area">
                                        <strong>Model No : </strong>
                                        {{$get->model_no}}
                                        <span></span>
                                    </div>
                                    <div class="area">
                                        <strong>PM Date : </strong>
                                        {{  \Carbon\Carbon::parse($get->pm_at)->toFormattedDateString() }}
                                        <span></span>
                                    </div>
                                    <div class="area">
                                        <strong>Warranty : </strong>
                                        {{ \Carbon\Carbon::parse($get->warranty)->toFormattedDateString() }}
                                        <span></span>
                                    </div>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#infoPanel" role="tab">Basic
                                        Information</a>
                                <li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ads" role="tab">More Details</a>
                                <li>
                                    <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#spareParts" role="tab">Spare Parts</a>
                                <li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#placementPanel"
                                       role="tab">Attachments</a>
                                <li>
                            </ul>

                            <form class="form-group company-form" role="form" method="POST" action="{{ route('pm.store') }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                
                                <div class="tab-content mt-2">
                                    <input type="hidden" name="device_id" value="{{$id}}">
                                    <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                        <br>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Assigned To</label>
                                                <select class="form-control" name="assigned_to">
                                                <option value="">-Select-</option>
                                                @foreach ($users as $value)
                                                <option value="{{ $value->id }}">{{ $value->name }} ({{ $value->email }})</option>
                                            @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <?php $list = json_decode($get['checklist']); ?>
                                        @if(!empty($list))
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Activity Done</label>
                                                <!-- <select class="form-control" name="">
                                                @foreach ($list as $value)
                                                    <option>{{ $check[$value] }}</option>
                                                @endforeach
                                                </select> -->
                                                @foreach ($list as $value)
                                                    <div class="checkbox">
                                                        <input id="checklist{{$value}}" type="checkbox" value="{{$value}}" name="pm_checklist[]">
                                                        <label for="checklist{{$value}}">
                                                            {{$check[$value] }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Activity Done</label>
                                                <input type="text" name="title"
                                                       class="form-control" placeholder="Activity Done"
                                                    >
                                            </div>
                                        </div>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Detailed Activity</label>
                                                <textarea name="report" class="form-control" rows="3" placeholder="Detailed Activity"></textarea>
                                            </div>
                                        </div>
                                        <br>
                                        <button class="btn btn-secondary pull-right" id="infoContinue">Continue</button>
                                    </div>

                                    <div class="tab-pane fade" id="ads" role="tabpanel"><br>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Cost Involved</label>
                                                <input type="text" name="cost"
                                                       class="form-control" placeholder="Cost"
                                                       max="50">
                                            </div>
                                        </div>

                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Remarks</label>
                                                <textarea name="remarks" class="form-control" rows="3" placeholder="Remarks"></textarea>
                                            </div>
                                        </div><br>
                                        <button class="btn btn-secondary pull-right" id="adsContinue">Continue</button>
                                    </div>

                                    <div class="tab-pane fade" id="spareParts" role="tabpanel"><br>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label class="control-label">Spare Parts</label>
                                                <select class="form-control ms" name="spare_part_id[]" multiple="multiple" id="spare_parts">
                                                @foreach ($spares as $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button class="btn btn-secondary pull-right" id="spareContinue">Continue</button><br>
                                    </div>

                                    <div class="tab-pane fade" id="placementPanel" role="tabpanel"><br>
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label for="exampleInputFile">Document</label>
                                                <input type="file" name="document" class="form-control-file"
                                                       id="exampleInputFile" aria-describedby="fileHelp">
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="progress mt-5">
                                <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width: 33%" aria-valuenow="20"
                                     aria-valuemin="0" aria-valuemax="100">Step 1 of 4
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-round" id="activate">Update Report
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('#infoContinue').click(function (e) {
            e.preventDefault();
            $('.progress-bar').css('width', '50%');
            $('.progress-bar').html('Step 2 of 4');
            $('#myTab a[href="#ads"]').tab('show');
        });

        $('#adsContinue').click(function (e) {
            e.preventDefault();
            $('.progress-bar').css('width', '75%');
            $('.progress-bar').html('Step 3 of 4');
            $('#myTab a[href="#spareParts"]').tab('show');
        });

        $('#spareContinue').click(function (e) {
            e.preventDefault();
            $('.progress-bar').css('width', '100%');
            $('.progress-bar').html('Step 4 of 4');
            $('#myTab a[href="#placementPanel"]').tab('show');
        });
    })

</script>

<script type="text/javascript">
        $('#spare_parts').multiSelect({
            selectableHeader: "<div class='custom-header'>Available</div>",
            selectionHeader: "<div class='custom-header'>Selected</div>"
        });
</script>

@endsection
@section('footer_scripts')