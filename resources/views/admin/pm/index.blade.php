@extends('layouts.app')
@section('title', 'PM List')
@section('page-style')

@section('content')
<!-- Basic Examples -->
<style type="text/css">
    #create:hover{
        color: #fff;
    }
</style>
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header" style="margin-bottom: 10px;">
                <h2><strong>PM List</strong> </h2>
                <ul class="header-dropdown">
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped table-hover js-basic-example dataTable" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>PM Order No</th>
                                <th>Activity Done</th>
                                <th>Device</th>
                                <th>Dept</th>
                                <th>Reported</th>
                                <th>Assigned</th>
                                <th>Closed</th>
                                <th>Progress</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')


    <script>
        $(document).ready(function () {
            $('.dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('pm.data') }}',
                columns: [
                    {data: 'wo_no', name: 'wo_no'},
                    {data: 'title', name: 'title'},
                    {data: 'device.name', name: 'device.name'},
                    {data: 'device.department.name', name: 'device.department.name'},
                    {data: 'pm', name: 'pm'},
                    {data: 'attend', name: 'attend'},
                    {data: 'close', name: 'close'},
                    {data: 'progress', name: 'progress'},
                    {data: 'actions', name: 'actions', searchable: false}
                ]
            });
        });

        // View Device
        $(document).on('click', '[data-view]', function () {
            var id = $(this).attr('data-view');
            if (Number($(this).attr('data-view')) > 0) {
                viewDevice(id);
            }
        });
        var device;

        // View Edit Page

        function viewEdit(id) {
            if (typeof device !== 'undefined')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "pms/" + id + "/edit",
                dataType: 'JSON',
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {
                    $('#showData').html(response.data);
                    $('.showModal').modal('show');
                }
            });
        }

        // Delete Device

        $(document).on('click', '[data-delete]', function () {
            var ele = $(this);
            var id = $(this).attr('data-delete');
            if (Number($(this).attr('data-delete')) > 0) {
                deleteDevice(id, ele);
            }
        });

        // Toastr             toastr["error"](" cannot be Empty", "");


    </script>
@endsection
