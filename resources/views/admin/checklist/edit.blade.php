<div class="modal-header bg-info">
    <h4 class="title" id="largeModalLabel" style="color: #fff;">Edit PM Checklist</h4>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
                        <div class="col-12">
                            <form class="form-group company-form" role="form" method="post" action="{{ route('checklist.update', $get->id) }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                <div class="tab-content mt-2">
                                    <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label for="title" class="control-label">Title*</label>
                                                <input type="text" name="title" class="form-control" placeholder="Title" max="50" id="title" value="{{$get->title}}">
                                            </div>
                                            <!-- <div class="col">
                                                <label for="device_id" class="control-label">Device</label>
                                                <select class="form-control" name="device_id" id="device_id">
                                                    <option value="" selected>Select Device</option>
                                                    @foreach ($devices as $value)
                                                        <option value="{{ $value->id }}" {{ $value->id == $get->device_id ? 'selected="selected"' : ''}}>
                                                            {{ $value->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div> -->                                           
                                        </div>

                                        <div class="form-group form-row col-sm-12">
                                            <div class="col">
                                                <label for="title" class="control-label">Device Name (Optional)</label>
                                                <input type="text" name="asset" class="form-control" placeholder="Device Name" max="50" id="asset" value="{{$get->asset}}">
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-info btn-round btn-block">Update Pm
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>    
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
</div>