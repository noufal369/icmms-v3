<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/multi-select/css/multi-select.css')}}">
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/multi-select/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/quick-search/js/jquery.quicksearch.js')}}"></script>

<style type="text/css">
    .custom-header {
    text-align: center;
    padding: 3px;
    background: #1cbfd0;
    color: #fff;
    font-size: 15px;
    height: 35px;
    }
    .space
        {
            margin-top: 15px;
        }
    .search-input{
        width: 388px;
        height: 35px;
    }
    .progress .progress-bar{
        height: 15px;
    }
    .add-btn, .remove_field {
        margin-top: 0px;
        height: 35px;
    }
</style>


<div class="modal-header bg-info">
    <h4 class="title" id="largeModalLabel" style="color: #fff;">Add Device</h4>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
    <div class="modal-body">
                    <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#infoPanel" role="tab">Basic
                            </a>
                        <li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#ads" role="tab">More Details</a>
                        <li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#placementPanel" role="tab">Attachments</a>
                        <li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#schedulePanel" role="tab">Additional Info</a>
                        <li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#sparesPanel" role="tab">Spare Parts</a>
                        <li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#checklistPanel" role="tab">PM Checklist</a>
                        <li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#reviewPanel" role="tab">Review</a>
                        <li>
                    </ul>

                    <form class="form-group company-form" role="form" method="post" action="{{ route('devices.store') }}"
                          enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="tab-content mt-2">
                            <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Name*</label>
                                        <input type="text" name="name" class="form-control" placeholder="Name"
                                               max="50">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Ownership</label>
                                        <input type="text" name="ownership" class="form-control"
                                               placeholder="Ownership" max="50">
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Serial No *</label>
                                        <div class="input-group">
                                        <input type="text" name="serial_no[]" class="form-control"
                                               placeholder="Serial No" required="required">
                                            <div class="input-group-btn"><button type="button" class="btn btn-success add-btn" id="add_serial"><i class="fa fa-plus-square"></i></button>  
                                            </div>
                                        </div>
                                        <div id="addSerial"></div>
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Model No</label>
                                        <input type="text" name="model_no" class="form-control"
                                               placeholder="Model No" max="30">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">GMDN</label>
                                        <input type="text" name="gmdn" class="form-control"
                                               placeholder="Global Medical Device Nomenclature" max="100">
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Power Spec</label>
                                        <input type="text" name="power_spec" class="form-control"
                                               placeholder="Power Spec" max="190">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Applicable Device</label>
                                        <input type="text" name="applicable_device" class="form-control"
                                               placeholder="Applicable Device" max="190">
                                    </div>
                                </div>
                                <br>
                                <button class="btn btn-info pull-right" id="infoContinue">Continue</button>
                            </div>
                            <div class="tab-pane fade" id="ads" role="tabpanel">

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Install Date</label>
                                        <input type="date" name="installed_at" class="form-control"
                                               placeholder="Install Date eg: 2016-12-31">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Warranty Date</label>
                                        <input type="date" name="warranty" class="form-control"
                                               placeholder="Warranty Period eg: 2016-12-31">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Calibration Date</label>
                                        <input type="date" name="calibration_at" class="form-control"
                                               placeholder="Calibration Date eg: 2016-12-31">
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">AMC Start Date</label>
                                        <input type="date" name="amc_start" class="form-control"
                                               placeholder="AMC Start Date eg: 2016-12-31">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">AMC Due Date</label>
                                        <input type="date" name="amc" class="form-control"
                                               placeholder="AMC Due Date eg: 2016-12-31">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">CMC Start Date</label>
                                        <input type="date" name="cmc_start" class="form-control"
                                               placeholder="CMC Start Date eg: 2016-12-31">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">CMC Due Date</label>
                                        <input type="date" name="cmc" class="form-control"
                                               placeholder="CMC Due Date eg: 2016-12-31">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">PM Date</label>
                                        <input type="date" name="pm" class="form-control"
                                               placeholder="PM Date eg: 2016-12-31">
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Purchase Date</label>
                                        <input type="date" name="purchased_at" class="form-control"
                                               placeholder="Purchase Date eg: 2016-12-31">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Purchase Cost</label>
                                        <input type="number" name="purchase_cost" class="form-control"
                                               placeholder="Purchase Cost">
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Purchase Order No</label>
                                        <input type="text" name="purchase_order_no" class="form-control"
                                               placeholder="Purchase Order No." max="50">
                                    </div>

                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">PM Frequency</label>
                                        <select class="form-control" name="pm_frequency">
                                            <option value="">Select PM Frequency</option>
                                            <option value="Yearly">Yearly</option>
                                            <option value="Half Yearly">Half Yearly</option>
                                            <option value="Quarterly">Quarterly</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Calibration Frequency</label>
                                        <select class="form-control" name="calibration_frequency">
                                            <option value="">Select PM Frequency</option>
                                            <option value="Yearly">Yearly</option>
                                            <option value="Half Yearly">Half Yearly</option>
                                            <option value="Quarterly">Quarterly</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Firmware Version</label>
                                        <input type="text" name="firmware_version" class="form-control"
                                               placeholder="Firmware Version" max="50">
                                    </div>
                                </div>

                                <br>
                                <button class="btn btn-info pull-right" id="adsContinue">Continue</button>

                            </div>
                            <div class="tab-pane fade" id="placementPanel" role="tabpanel">

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Adverse Events</label>
                                        <textarea class="form-control" rows="3" name="adverse_events"
                                                  placeholder="Adverse Events" maxlength="300"></textarea>
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Inspection Details</label>
                                        <textarea class="form-control" rows="3" name="inspection_details"
                                                  placeholder="Inspection Details" maxlength="300"></textarea>
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Other Info</label>
                                        <textarea class="form-control" rows="3" name="other_info"
                                                  placeholder="Other Info" maxlength="300"></textarea>
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Comments</label>
                                        <textarea class="form-control" rows="3" name="comments" placeholder="Comments"
                                                  maxlength="300"></textarea>
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label for="exampleInputFile">Document</label>
                                        <input type="file" name="document" class="form-control-file"
                                               id="exampleInputFile" aria-describedby="fileHelp">
                                        <small id="fileHelp" class="form-text text-muted">Select a file to use as the
                                            fullscreen ad image. Please ensure the size is at least 1080x1920 with a
                                            9:16 (portrait) aspect ratio.
                                        </small>
                                    </div>
                                    <div class="col">
                                        <label for="exampleInputFile">Photo</label>
                                        <input type="file" name="image" class="form-control-file" id="exampleInputImage"
                                               aria-describedby="fileHelp">
                                        <small id="fileHelp" class="form-text text-muted">Select a file to use as the
                                            banner ad image. Please ensure the size is exactly 1080x450 for proper
                                            rendering.
                                        </small>
                                    </div>
                                </div>

                                <br>
                                <button class="btn btn-info pull-right" id="placementContinue">Continue</button>

                            </div>
                            <div class="tab-pane fade" id="schedulePanel" role="tabpanel">

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Department*</label>
                                        <select class="form-control" name="department_id">
                                            <option value="" selected>Select Department</option>
                                            @foreach ($departments as $value)
                                                <option value="{{ $value->id }}" {{ old('department')== $value->id ? 'selected="selected"' : ''}}>{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Supplier</label>
                                        <select class="form-control" name="supplier_id">
                                            <option value="">Select Supplier</option>
                                            @foreach ($suppliers as $value)
                                                <option value="{{ $value->id }}" {{ old('supplier')== $value->id ? 'selected="selected"' : ''}}>{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Manufacturer</label>
                                        <select class="form-control" name="manufacturer_id">
                                            <option value="">Select Manufacturer</option>
                                            @foreach ($manufacturers as $value)
                                                <option value="{{ $value->id }}" {{ old('manufacturer')== $value->id ? 'selected="selected"' : ''}}>{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label for="service_provider_id" class="control-label">Service Provider</label>
                                        <select class="form-control" name="service_provider_id">
                                            <option value="">Select Service Provider</option>
                                            @foreach ($serviceProviders as $value)
                                                <option value="{{ $value->id }}" {{ old('service_provider')== $value->id ? 'selected="selected"' : ''}}>{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label class="control-label">Risk Classification</label>
                                        <select class="form-control" name="category_id">
                                            <option value="">Select Risk Classification*</option>
                                            @foreach ($categories as $value)
                                                <option value="{{ $value->id }}" {{ old('category_id')== $value->id ? 'selected="selected"' : ''}}>{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col">
                                        <label for="engineers" class="control-label">Assigned Engineer</label>
                                        <select class="form-control" name="preferred_engineer">
                                            <option value="">Select an Engineer</option>
                                            @foreach ($users as $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    {{--<div class="col">--}}
                                    {{--<label class="control-label">Accessories</label>--}}
                                    {{--<select class="form-control" name="accessories[]" multiple="">--}}
                                    {{--<option value="">Select Accessories</option>--}}
                                    {{--</select>--}}
                                    {{--</div>--}}
                                </div>

                                <br>
                                <button class="btn btn-info pull-right" id="scheduleContinue">Continue</button>

                            </div>

                            <div class="tab-pane fade" id="sparesPanel" role="tabpanel">
                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">Spare Parts</label>
                                        <select class="form-control" name="spare_parts[]" id="spare_parts" multiple="multiple">
                                            @foreach ($spare_parts as $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <button class="btn btn-info pull-right" id="sparesContinue">Continue</button>
                            </div>

                            <div class="tab-pane fade" id="checklistPanel" role="tabpanel">
                                <div class="form-group form-row col-sm-12">
                                    <div class="col">
                                        <label class="control-label">PM Checklist</label>
                                        <select class="form-control" name="checklist[]" id="checklist" multiple="multiple">
                                            @foreach ($checklist as $value)
                                                <option value="{{ $value->id }}">{{ $value->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <button class="btn btn-info pull-right" id="checklistContinue">Continue</button>
                            </div>
                            <div class="tab-pane fade" id="reviewPanel" role="tabpanel">
                                <button type="submit" class="btn btn-info btn-round btn-block" id="activate">Create Device
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="progress mt-5">
                        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width: 14%" aria-valuenow="20"
                             aria-valuemin="0" aria-valuemax="100">Step 1 of 7
                        </div>
                    </div>
                </div>

            </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
</div>
<script>
        $(function () {
            $('#infoContinue').click(function (e) {
                e.preventDefault();
                $('.progress-bar').css('width', '29%');
                $('.progress-bar').html('Step 2 of 7');
                $('#myTab a[href="#ads"]').tab('show');
            });

            $('#adsContinue').click(function (e) {
                e.preventDefault();
                $('.progress-bar').css('width', '43%');
                $('.progress-bar').html('Step 3 of 7');
                $('#myTab a[href="#placementPanel"]').tab('show');
            });

            $('#placementContinue').click(function (e) {
                e.preventDefault();
                $('.progress-bar').css('width', '58%');
                $('.progress-bar').html('Step 4 of 7');
                $('#myTab a[href="#schedulePanel"]').tab('show');
            });

            $('#scheduleContinue').click(function (e) {
                e.preventDefault();
                $('.progress-bar').css('width', '72%');
                $('.progress-bar').html('Step 5 of 7');
                $('#myTab a[href="#sparesPanel"]').tab('show');
            });

            $('#sparesContinue').click(function (e) {
                e.preventDefault();
                $('.progress-bar').css('width', '87%');
                $('.progress-bar').html('Step 6 of 7');
                $('#myTab a[href="#checklistPanel"]').tab('show');
            });

            $('#checklistContinue').click(function (e) {
                e.preventDefault();
                $('.progress-bar').css('width', '100%');
                $('.progress-bar').html('Step 7 of 7');
                $('#myTab a[href="#reviewPanel"]').tab('show');
            });
        })

    </script>

    <script type="text/javascript">
        var count = 0;
        $('#checklist').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Search...'>",
            selectionHeader: "<div class='custom-header'>Selected : <span id='counter2'>0</span></div>",

            afterInit: function(ms){
            var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
            .on('keydown', function(e){
              if (e.which === 40){
                that.$selectableUl.focus();
                return false;
              }
            });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
            .on('keydown', function(e){
              if (e.which == 40){
                that.$selectionUl.focus();
                return false;
              }
            });
          },
          afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
            count = count + 1;
            $('#counter2').text(count);
          },
          afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
            count = count - 1;
            $('#counter2').text(count);
          }
        });
    </script>

    <script type="text/javascript">
        var count2 = 0;
        $('#spare_parts').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Search...'>",
            selectionHeader: "<div class='custom-header'>Selected : <span id='counter'>0</span></div>",

            afterInit: function(ms){
            var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
            .on('keydown', function(e){
              if (e.which === 40){
                that.$selectableUl.focus();
                return false;
              }
            });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
            .on('keydown', function(e){
              if (e.which == 40){
                that.$selectionUl.focus();
                return false;
              }
            });
          },
          afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
            count2 = count2 + 1;
            $('#counter').text(count2);
          },
          afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
            count2 = count2 - 1;
            $('#counter').text(count2);
          }
        });
    </script>

    <script>
 
        $(document).ready(function() {
        var max_fields      = 6; //maximum input boxes allowed
        var wrapper         = $("#addSerial"); //Fields wrapper
        var add_button      = $("#add_serial"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-group space"><input type="text" class="form-control" placeholder="Serial No" name="serial_no[]" id="serial_no" required="required"/><a href="javascript:void(0)" class="btn btn-danger remove_field"><span class="fa fa-minus-square"></span></a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
    });

    </script>