@extends('layouts.app')
@section('title', 'Equipments')
@section('page-style')

@section('content')
<!-- Basic Examples -->
<style type="text/css">
    #create:hover{
        color: #fff;
    }
</style>
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header" style="margin-bottom: 10px;">
                <h2><strong>Equipments</strong> </h2>
                <ul class="header-dropdown">
                    <button id="create" type="button" class="btn bg-pink" onclick="viewCreate()">Add +
                    </button>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped table-hover js-basic-example dataTable" style="width: 100%;font-size: 13px;">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Dept</th>
                                <th>Sl No</th>
                                <th>Model No</th>
                                <!-- <th>AMC</th>
                                <th>CMC</th> -->
                                <th>PM</th>
                                <th>Warranty</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')
    <script>

        // DataTable
        $(document).ready(function () {
            $('.dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('devices.data') }}',
                columns: [
                    {data: 'code', name: 'code'},
                    {data: 'name', name: 'name'},
                    {data: 'department.name', name: 'department.name'},
                    {data: 'serial_no', name: 'serial_no'},
                    {data: 'model_no', name: 'model_no'},
                    // {data: 'amc', name: 'amc'},
                    // {data: 'cmc', name: 'cmc'},
                    {data: 'pm', name: 'pm'},
                    {data: 'warranty', name: 'warranty'},
                    {data: 'actions', name: 'actions', searchable: false}
                ]
            });
        });


        // View Device
        $(document).on('click', '[data-view]', function () {
            var id = $(this).attr('data-view');
            if (Number($(this).attr('data-view')) > 0) {
                viewDevice(id);
            }
        });
        var device;

        function viewDevice(id) {
            if (typeof(id) !== "undefined") {
                if (id > 0) {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        type: 'GET',
                        url: "devices/" + id,
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            $('#showData').html(response.data);
                            $('.showModal').modal('show');
                        }
                    });
                }
            }
        }

        // View Create Page

        function viewCreate() {
            if (typeof device !== 'undefined')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "devices/create/",
                dataType: 'JSON',
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {                   
                    $('.modal-lg').addClass('modal-xl');
                    $('.modal-lg').removeClass('modal-lg');
                    $('.modal-xl').css('width', '950px');
                    $('#showData').html(response.data);
                    $('.showModal').modal('show');
                }
            });
        }

        // View Edit Page

        function viewEdit(id) {
            if (typeof device !== 'undefined')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "devices/" + id + "/edit",
                dataType: 'JSON',
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {
                    $('.modal-lg').addClass('modal-xl');
                    $('.modal-lg').removeClass('modal-lg');
                    $('.modal-xl').css('width', '950px');
                    $('#showData').html(response.data);
                    $('.showModal').modal('show');
                }
            });
        }

        // Delete Device

        $(document).on('click', '[data-delete]', function () {
            var ele = $(this);
            var id = $(this).attr('data-delete');
            if (Number($(this).attr('data-delete')) > 0) {
                deleteDevice(id, ele);
            }
        });

// Toastr             toastr["error"](" cannot be Empty", "");

        function deleteDevice(id, ele) {
            var msgTxt;
            var msgTitle;
            if (ele.find('i').hasClass('fa-recycle')) {
                msgTxt = "Device Activated";
                msgTitle = "Are you sure you want to activate ";
            } else {
                msgTxt = "Device Deleted";
                msgTitle = "Are you sure you want to delete ";
            }
            swal({
                title: msgTitle,
                text: "This device will be disabled and removed from listing.",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                setTimeout(function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        type: 'DELETE',
                        url: "devices/" + id,
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            if (response.status === 201) {
                                if (ele.find('i').hasClass('fa-recycle')) {
                                    ele.find('i').attr('class', 'fa fa-trash-o').attr('title', 'Delete Device');
                                    swal({title: "Device deletion failed", type: "success"});
                                } else {
                                    ele.find('i').attr('class', 'fa fa-recycle').attr('title', 'Activate Device');
                                    swal({title: "Device deletion failed", type: "success"});
                                }
                                swal({title: msgTxt, type: "success"});
                            } else {
                                swal({title: "Device deletion failed", type: "error"});
                            }
                        }
                    });
                }, 1000);
            });
        }
    </script>
@endsection
