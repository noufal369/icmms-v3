@extends('layouts.app')
@section('title', 'Equipment Details')
@section('page-style')
@section('content')
<style>
.img-fluid{
    width: 207px;
    height: 207px;
}
.thumb{
    width: 40px;
    height: 40px;
}

.my-data div{
    float:left;
    width:50%;
    /*text-align:center;*/
    position:relative;
    padding-bottom: 10px;
}
.start{
    width: 15%;
}
.end{
    width: 35%;
}
</style>
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-12">
                        <div class="preview preview-pic tab-content">
                            <div class="tab-pane active" id="product_1"><img src="{{ URL::to('/') }}/uploads/images/{{$get->image}}" class="img-fluid" alt="" /></div>
                            <div class="tab-pane" id="product_2"><img src="{{ URL::to('/') }}/uploads/qr_codes/{{$get->qr_code}}" class="img-fluid" alt=""/></div>
                        </div>
                        <ul class="preview thumbnail nav nav-tabs">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#product_1"><img class="thumb" src="{{ URL::to('/') }}/uploads/images/{{$get->image}}" alt=""/></a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#product_2"><img class="thumb" src="{{ URL::to('/') }}/uploads/qr_codes/{{$get->qr_code}}" alt=""/></a></li>
                        </ul>
                    </div>
                    <div class="col-xl-9 col-lg-8 col-md-12">
                        <div class="product details">
                            <h3 class="product-title mb-0">{{ $get->name }}</h3>
                            <button id="edit" type="button" class="btn btn-info" onclick="viewEdit({{$get->id}})" style="float: right;">Edit
                             </button>
                            <h5 class="price mt-0">ID: <span class="col-purple">{{ $get->code }}</span></h5>
                            <h5 class="price mt-0">Risk Classification:
                                @if($get->category_id == 1 )
                                    <span class="col-red">{{ $get->category->name }}</span>
                                @elseif($get->category_id == 2)
                                    <span class="col-amber">{{ $get->category->name }}</span>
                                @elseif($get->category_id == 3)
                                    <span class="col-green">{{ $get->category->name }}</span>
                                @endif
                            </h5>
                            <hr>
                            <p class="product-description">
                                <div class="my-data">
                                    <div class="area">
                                        <strong>Serial No : </strong>{{ $get->serial_no }}
                                        <span></span>
                                    </div>
                                    <div class="area">
                                        <strong>Manufacturer : </strong>{{ isset($get->manufacturer->name)?$get->manufacturer->name : '' }}
                                    </div>
                                    <div class="area">
                                        <strong>Model No : </strong>{{ $get->model_no }}
                                    </div>
                                    <div class="area">
                                        <strong>Cost : </strong>{{ $get->purchase_cost }}
                                    </div>
                                    <div class="area">
                                        <strong>Warranty : </strong>{{ \Carbon\Carbon::parse($get->warranty)->toFormattedDateString() }}
                                    </div>
                                    <div class="area">
                                        <strong>Supplier : </strong>{{ $get->supplier->name }}
                                    </div>
                                    <div class="area">
                                        <strong>Installation : </strong>{{ \Carbon\Carbon::parse($get->installed_at)->toFormattedDateString() }}
                                    </div>
                                    <div class="area">
                                        <strong>Service Provider : </strong>{{ isset($get->service_provider->name)? $get->service_provider->name : '' }}
                                    </div>
                                    <div class="area">
                                        <strong>Age of Device : </strong>
                                        {{$get->age_of_device}}
                                    </div>
                                    <div class="area">
                                        <strong>Preferred Engineer : </strong>
                                        @if(!is_null($get->preferred_engineer))
                                            {{$userArray[$get->preferred_engineer]}}
                                        @endif
                                    </div>
                                </div>
                            </p>

                            <div class="action">
                                <button class="btn btn-success waves-effect" type="button">Active</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#description">Details</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#review">History</a></li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <div class="tab-content">
                    <div class="tab-pane active" id="description">

                        <div class="table-responsive">
                        <table class="table table-sm product_item_list c_table mb-0">
                            <tbody>
                                <tr>
                                    <td class="start">AMC Start Date : </td>
                                    <td class="end">{{ \Carbon\Carbon::parse($get->amc_start)->toFormattedDateString() }}</td>
                                    <td class="start">AMC Due Date : </td>
                                    <td class="end">{{ \Carbon\Carbon::parse($get->amc)->toFormattedDateString() }}</td>
                                </tr>
                                <tr>
                                    <td class="start">CMC Start Date : </td>
                                    <td class="end">{{ \Carbon\Carbon::parse($get->cmc_start)->toFormattedDateString() }}</td>
                                    <td class="start">CMC Due Date : </td>
                                    <td class="end">{{ \Carbon\Carbon::parse($get->cmc)->toFormattedDateString() }}</td>
                                </tr>
                                <tr>
                                    <td class="start">PM Date : </td>
                                    <td class="end">{{ \Carbon\Carbon::parse($get->pm)->toFormattedDateString() }} - {{ $get->pm_frequency }}</td>
                                    <td class="start">Calibration Date : </td>
                                    <td class="end">{{ \Carbon\Carbon::parse($get->calibration_at)->toFormattedDateString() }} - {{ $get->calibration_frequency }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Power Spec : </td>
                                    <td class="end">{{ $get->power_spec }}</td>
                                    <td class="start">Purchase Order No : </td>
                                    <td class="end">{{ $get->purchase_order_no }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Firmware Version : </td>
                                    <td class="end">{{ $get->firmware_version }}</td>
                                    <td class="start">Ownership : </td>
                                    <td class="end">{{ $get->ownership }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Inspection Details : </td>
                                    <td class="end">{{ $get->inspection_detail }}</td>
                                    <td class="start">Purchased On : </td>
                                    <td class="end">{{ $get->purchased_at }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Applicable Device : </td>
                                    <td class="end">{{ $get->applicable_device }}</td>
                                    <td class="start">Adverse Events : </td>
                                    <td class="end">{{ $get->adverse_events }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Other Information : </td>
                                    <td class="end">{{ $get->other_info }}</td>
                                    <td class="start">Comments : </td>
                                    <td class="end">{{ $get->comments }}</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="review">
                    @if(!empty($get->complaints))
                    @foreach($get->complaints as $item)
                    <div class="panel-group" id="accordion_{{$item->id}}" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-primary">
                            <div class="panel-heading" role="tab" id="headingOne_{{$item->id}}">
                                <h6> {{$item->title}} - {{ \Carbon\Carbon::parse($item->complained_at)->toFormattedDateString() }}
                                    <a style="float: right;color:#fff;" class="btn btn-info btn-sm btn-round" role="button" data-toggle="collapse" data-parent="#accordion_{{$item->id}}" href="#collapseOne_{{$item->id}}" aria-expanded="true" aria-controls="collapseOne_{{$item->id}}"> View </a>
                                </h6>
                            </div><br>
                            <div id="collapseOne_{{$item->id}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_{{$item->id}}">
                                <div class="panel-body">
                                   <div class="table-responsive">
                        <table class="table table-sm product_item_list c_table mb-0">
                            <tbody>
                                <tr>
                                    <td class="start">Title : </td>
                                    <td class="end">{{ $item->title }}</td>
                                    <td class="start">Work Order No : </td>
                                    <td class="end">{{ $item->order_no }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Severity : </td>
                                    <td class="end">@if($item->severity == 0)
                                        <p class="text-success">Minor</p>
                                        @elseif($item->severity == 1)
                                        <p class="text-warning">Major</p>
                                        @elseif($item->severity == 2)
                                        <p class="text-danger">Critical</p>
                                        @endif
                                    </td>
                                    <td class="start">Error Code : </td>
                                    <td class="end">
                                        @foreach ($errors as $value)
                                            @if($value->id == $item->error_id)
                                                {{ $value->name }}
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td class="start">Reported On : </td>
                                    <td class="end">{{$item->complained_at->toFormattedDateString()}}</td>
                                    <td class="start">Reported By : </td>
                                    <td class="end">{{ $userArray[$item->complained_by] }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Responded At : </td>
                                    <td class="end">{{ isset($item->responded_at)? $item->responded_at->toFormattedDateString() : ''}}</td>
                                    <td class="start">Responded By : </td>
                                    <td class="end">{{ isset($item->responded_by)? $userArray[$item->responded_by] : ''}}</td>
                                </tr>
                                <tr>
                                    <td class="start">Assigned To : </td>
                                    <td class="end">
                                        @foreach ($users as $value)
                                            <?php if(!is_null($item->assigned_to)) {
                                                if(in_array($value->id, json_decode($item->assigned_to))) { ?>
                                                {{$value->name}} , 

                                            <?php }
                                            } ?>
                                        @endforeach
                                    </td>
                                    <td class="start">Spare Parts : </td>
                                    <td class="end">
                                        @if(!is_null($item->spare_part_id))
                                            @foreach($spare_parts as $value)
                                                @if(in_array($value->id, json_decode($item->spare_part_id)))
                                                    {{$value->name}} , &nbsp;
                                               @endif
                                            @endforeach
                                        @endif
                                    </td>
                                </tr>
                            
                                <tr>
                                    <td class="start">Closed On : </td>
                                    <td class="end">{{ isset($item->closed_at)? $item->closed_at->toFormattedDateString() : ''}}</td>
                                    <td class="start">Closed By : </td>
                                    <td class="end">{{ isset($item->closed_by)? $userArray[$item->closed_by] : ''}}</td>
                                </tr>
                                <tr>
                                    <td class="start">Expected <br>Completion Time : </td>
                                    <td class="end">{{ $item->expected_completion_time }}</td>
                                    <td class="start">Reason For <br>Pending ( If Any ) : </td>
                                    <td class="end">{{ $item->reason_for_pending }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Cost Involved : </td>
                                    <td class="end">{{ $item->cost }}</td>
                                    <td class="start">Remarks : </td>
                                    <td class="end">{{ $item->more_info }}</td>
                                </tr>
                                <tr>
                                    <td class="start">Response Time : </td>
                                    <td class="end"></td>
                                    <td class="start">Completion Time : </td>
                                    <td class="end"></td>
                                </tr>
                                <tr>
                                    <td class="start">Down Time : </td>
                                    <td class="end"></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

<script type="text/javascript">

    var device;

    function viewEdit(id) {
            if (typeof device !== 'undefined')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "/devices/" + id + "/edit",
                dataType: 'JSON',
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {
                    $('.modal-lg').addClass('modal-xl');
                    $('.modal-lg').removeClass('modal-lg');
                    $('.modal-xl').css('width', '950px');
                    $('#showData').html(response.data);
                    $('.showModal').modal('show');
                }
            });
        }
</script>

@endsection
