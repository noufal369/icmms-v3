@extends('layouts.app')
@section('title', 'Dashboard')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/charts-c3/plugin.css')}}" />
<link rel="stylesheet" href="{{asset('assets/plugins/morrisjs/morris.min.css')}}" />
@stop
@section('content')

<style type="text/css">
    .view-btn{
        font-size: 1.2em;
    }
</style>

<div class="row clearfix">
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card widget_2 big_icon traffic">
            <div class="body">
                <h6>Equipments</h6>
                <h2>60 <small class="info">Equipments</small></h2>
                <small>View</small>
                <div class="progress">
                    <div class="progress-bar l-amber" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card widget_2 big_icon sales">
            <div class="body">
                <h6>Customers</h6>
                <h2>12 <small class="info">Customers</small></h2>
                <small>View</small>
                <div class="progress">
                    <div class="progress-bar l-blue" role="progressbar" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card widget_2 big_icon email">
            <div class="body">
                <h6>Inventories</h6>
                <h2>30 <small class="info">Inventories</small></h2>
                <small>View</small>
                <div class="progress">
                    <div class="progress-bar l-purple" role="progressbar" aria-valuenow="39" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card widget_2 big_icon domains">
            <div class="body">
                <h6>Accessories</h6>
                <h2>18 <small class="info">Accessories</small></h2>
                <small>View</small>
                <div class="progress">
                    <div class="progress-bar l-green" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-6 col-sm-6 col-6 text-center">
        <div class="card">
            <div class="body">
                <input type="text" class="knob" value="42" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#00adef" readonly>
                <p>Upcoming PM</p>
                <div class="d-flex bd-highlight text-center mt-4">
                    <div class="flex-fill bd-highlight">
                        <small class="text-muted">Today</small>
                        <h5 class="mb-0">25</h5>
                    </div>
                    <div class="flex-fill bd-highlight">
                        <button class="btn btn-danger view-btn btn-raised l-turquoise waves-effect" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
                            aria-controls="collapseExample"> View </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-6 text-center">
        <div class="card">
            <div class="body">
                <input type="text" class="knob" value="81" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#ee2558" readonly>
                <p>Upcoming Warranty</p>
                <div class="d-flex bd-highlight text-center mt-4">
                    <div class="flex-fill bd-highlight">
                        <small class="text-muted">Today</small>
                        <h5 class="mb-0">34</h5>
                    </div>
                    <div class="flex-fill bd-highlight">
                        <button class="btn btn-danger view-btn btn-raised l-turquoise waves-effect" type="button"> View </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-6 text-center">
        <div class="card">
            <div class="body">
                <input type="text" class="knob" value="62" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#8f78db" readonly>
                <p>Upcoming AMC</p>
                <div class="d-flex bd-highlight text-center mt-4">
                    <div class="flex-fill bd-highlight">
                        <small class="text-muted">Today</small>
                        <h5 class="mb-0">25</h5>
                    </div>
                    <div class="flex-fill bd-highlight">
                        <button class="btn btn-danger view-btn btn-raised l-turquoise waves-effect" type="button"> View </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-6 text-center">
        <div class="card">
            <div class="body">
                <input type="text" class="knob" value="38" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#f67a82" readonly>
                <p>Upcoming CMC</p>
                <div class="d-flex bd-highlight text-center mt-4">
                    <div class="flex-fill bd-highlight">
                        <small class="text-muted">Today</small>
                        <h5 class="mb-0">15</h5>
                    </div>
                    <div class="flex-fill bd-highlight">
                        <button class="btn btn-danger view-btn btn-raised l-turquoise waves-effect" type="button"> View </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 collapse" id="collapseExample">
        <div class="card">
            <div class="header" style="margin-bottom: 10px;">
                <h2><strong>Upcoming PM</strong> </h2>
                <ul class="header-dropdown">
                </ul>
            </div>
            <div class="body">
                <!-- <div> -->
                    <table class="table table-sm table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Model No</th>
                                <th>PM Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($pm_devices))
                        @foreach($pm_devices as $device)
                            <tr>
                                <td><a href="">{{ $device['code'] }}</a></td>
                                <td>{{ $device['name'] }}</td>
                                <td>{{ $device['model_no'] }}</td>
                                <td><span class="badge badge-info"> {{  \Carbon\Carbon::parse($device['pm'])->toFormattedDateString() }}</span></td>
                                <td><a href="{{ route('devices.create-pm', $device['id']) }}" class="btn btn-primary btn-sm btn-icon btn-icon-mini btn-round"><i class="fa fa-pencil-square-o" data-name="edit" title="Edit" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-8 col-md-12">
        <div class="card product-report">
            <div class="header">
                <!-- <h2><strong>Annual</strong> Report</h2> -->
                <ul class="header-dropdown">
                </ul>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="icon xl-amber m-b-15"><i class="zmdi zmdi-chart-donut"></i></div>
                        <div class="col-in">
                            <small class="text-muted mt-0">Maintenance</small>
                            <h4 class="mt-0">$4,516</h4>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="icon xl-blue m-b-15"><i class="zmdi zmdi-chart"></i></div>
                        <div class="col-in">
                            <small class="text-muted mt-0">Accessories</small>
                            <h4 class="mt-0">$6,481</h4>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="icon xl-purple m-b-15"><i class="zmdi zmdi-card"></i></div>
                        <div class="col-in">
                            <small class="text-muted mt-0">Spare Parts</small>
                            <h4 class="mt-0">$3,915</h4>
                        </div>
                    </div>
                </div>
                <div id="area_chart"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="header">
                <!-- <h2><strong>Distribution</strong></h2> -->
                <ul class="header-dropdown">
                </ul>
            </div>
            <div class="body text-center">
                <div id="chart-pie" class="c3_chart d_distribution"></div>
                <button class="btn btn-primary mt-4 mb-4">View More</button>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="header">
                <!-- <h2><strong>Popular</strong> Categories</h2> -->
                <ul class="header-dropdown">
                </ul>
            </div>
            <div class="body">
                <div id="chart-bar" style="height: 16rem"></div>
            </div>
        </div>
    </div>
</div>


@stop
@section('page-script')
<script src="{{asset('assets/bundles/morrisscripts.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/jvectormap.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/knob.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/c3.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/index.js')}}"></script>
<script src="{{asset('assets/js/pages/ecommerce.js')}}"></script>
<script src="{{asset('assets/js/pages/blog/blog.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/jquery-knob.min.js')}}"></script>

<script>
    $(document).ready(function () {
            $('.dataTable').DataTable({
                // "scrollY": "250px",
                "scrollCollapse": true,
            });
            $('.dataTables_length').addClass('bs-select');
        });
</script>

@stop
