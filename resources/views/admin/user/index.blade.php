@extends('layouts.app')
@section('title', 'Users')
@section('page-style')

@section('content')
<!-- Basic Examples -->
<style type="text/css">
    #create:hover{
        color: #fff;
    }
</style>
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header" style="margin-bottom: 10px;">
                <h2><strong>Users</strong> </h2>
                <ul class="header-dropdown">
                    <button id="create" type="button" class="btn bg-pink" onclick="viewCreate()">Add +
                    </button>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped table-hover js-basic-example dataTable" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Updated</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

    <script>

        // DataTable
        $(document).ready(function () {
            $('.dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('users.data') }}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false}
                ]
            });
        });


        // View User
        $(document).on('click', '[data-view]', function () {
            var id = $(this).attr('data-view');
            if (Number($(this).attr('data-view')) > 0) {
                viewUser(id);
            }
        });
        var device;

        function viewUser(id) {
            if (typeof(id) !== "undefined") {
                if (id > 0) {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        type: 'GET',
                        url: "users/" + id,
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            $('#showData').html(response.data);
                            $('.showModal').modal('show');
                        }
                    });
                }
            }
        }

        // View Create Page

        function viewCreate() {
            if (typeof device !== 'undefined')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "users/create/",
                dataType: 'JSON',
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {
                    $('#showData').html(response.data);
                    $('.showModal').modal('show');
                }
            });
        }

        // View Edit Page

        function viewEdit(id) {
            if (typeof device !== 'undefined')
                device.abort();
            device = $.ajax({
                type: 'GET',
                url: "users/" + id + "/edit",
                dataType: 'JSON',
                async: true,
                beforeSend: function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                },
                success: function (response) {
                    $('#showData').html(response.data);
                    $('.showModal').modal('show');
                }
            });
        }

        // Delete User

        $(document).on('click', '[data-delete]', function () {
            var ele = $(this);
            var id = $(this).attr('data-delete');
            if (Number($(this).attr('data-delete')) > 0) {
                deleteUser(id, ele);
            }
        });

        // Toastr             toastr["error"](" cannot be Empty", "");

        function deleteUser(id, ele) {
            var msgTxt;
            var msgTitle;
            if (ele.find('i').hasClass('fa-recycle')) {
                msgTxt = "User Activated";
                msgTitle = "Are you sure you want to activate ";
            } else {
                msgTxt = "User Deleted";
                msgTitle = "Are you sure you want to delete ";
            }
            swal({
                title: msgTitle,
                text: "This device will be disabled and removed from listing.",
                type: "error",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                setTimeout(function () {
                    if (typeof device !== 'undefined')
                        device.abort();
                    device = $.ajax({
                        type: 'DELETE',
                        url: "users/" + id,
                        dataType: 'JSON',
                        async: true,
                        beforeSend: function () {
                            if (typeof device !== 'undefined')
                                device.abort();
                        },
                        success: function (response) {
                            if (response.status === 201) {
                                if (ele.find('i').hasClass('fa-recycle')) {
                                    ele.find('i').attr('class', 'fa fa-trash-o').attr('title', 'Delete User');
                                    swal({title: "User deletion failed", type: "success"});
                                } else {
                                    ele.find('i').attr('class', 'fa fa-recycle').attr('title', 'Activate User');
                                    swal({title: "User deletion failed", type: "success"});
                                }
                                swal({title: msgTxt, type: "success"});
                            } else {
                                swal({title: "User deletion failed", type: "error"});
                            }
                        }
                    });
                }, 1000);
            });
        }
    </script>
@endsection
