<div class="modal-header bg-info">
    <h4 class="title" id="largeModalLabel" style="color: #fff;">Edit User</h4>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
                        <div class="col-12">
                            <form class="form-group company-form" role="form" method="POST"
                                  action="{{ route('users.update', $get->id) }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="name" class="control-label">Name*</label>
                                            <input type="text" id="name" name="name" class="form-control"
                                                   value="{{$get->name}}" max="50">
                                        </div>
                                        <div class="col">
                                            <label for="phone" class="control-label">Phone Number*</label>
                                            <input type="number" id="phone" name="phone" class="form-control"
                                                   placeholder="Phone Number" value="{{$get->phone}}">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="address" class="control-label">Address</label>
                                            <textarea name="address" class="form-control" rows="3">{{$get->address}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="email" class="control-label">Email</label>
                                            <input type="email" id="email" name="email" class="form-control"
                                                   value="{{$get->email}}" max="190">
                                        </div>
                                        <div class="col">
                                            <label for="password" class="control-label">Password</label>
                                            <input type="password" id="password" name="password" class="form-control"
                                                   value="{{$get->password}}" max="50">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="user_type" class="control-label">User Type</label>
                                            <select name="user_type_id" class="form-control" id="user_type_id" required="required">
                                                <option value="">-Select-</option>
                                                @foreach($usertype as $item)
                                                    <option value="{{$item->id}}" {{ $item->id == $get->user_type_id ? 'selected="selected"' : ''}}>{{$item->user_type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="department" class="control-label">Department</label>
                                            <select name="department_id" class="form-control" id="department" required="required">
                                                <option value="">-Select-</option>
                                                @foreach($department as $item)
                                                    <option value="{{$item->id}}" {{ $item->id == $get->department_id ? 'selected="selected"' : ''}}>{{$item->name}}</option>
                                                @endforeach  
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="company_id" class="control-label">Organization</label>
                                            <select name="company_id" class="form-control" id="company_id" required="required">
                                                <option value="">-Select-</option>
                                                @foreach($company as $item)
                                                    <option value="{{$item->id}}" {{ $item->id == $get->company_id ? 'selected="selected"' : ''}}>{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="name" class="control-label">Extension Number</label>
                                            <input type="text" id="extension_number" name="extension_number" class="form-control"
                                                   placeholder="Extension Number" value="{{$get->extension_number}}">
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-info btn-block btn-round" id="activate">Update
                                        User
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>    
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
</div>