<div class="modal-header bg-info">
    <h4 class="title" id="largeModalLabel" style="color: #fff;">Add User</h4>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
    <div class="modal-body">
        <div class="row">
                <div class="col-12">
                    <form class="form-group company-form" role="form" method="post"
                                  action="{{ route('users.store') }}"
                                  enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                            <div class="form-group form-row col-sm-12">
                                <div class="col">
                                    <label for="name" class="control-label">Name*</label>
                                    <input type="text" id="name" name="name" class="form-control"
                                                   placeholder="Name" max="50">
                                </div>
                                <div class="col">
                                    <label for="phone" class="control-label">Phone Number*</label>
                                    <input type="number" id="phone" name="phone" class="form-control"
                                                   placeholder="Phone Number">
                                </div>
                            </div>

                            <div class="form-group form-row col-sm-12">
                                <div class="col">
                                    <label for="address" class="control-label">Address</label>
                                    <textarea name="address" class="form-control" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="form-group form-row col-sm-12">
                                <div class="col">
                                    <label for="email" class="control-label">Email</label>
                                    <input type="email" id="email" name="email" class="form-control"
                                                   placeholder="Email" max="190">
                                </div>
                                <div class="col">
                                    <label for="password" class="control-label">Password</label>
                                    <input type="password" id="password" name="password" class="form-control"
                                                   placeholder="Password" max="50">
                                </div>
                            </div>

                            <div class="form-group form-row col-sm-12">
                                <div class="col">
                                    <label for="user_type" class="control-label">User Type</label>
                                    <select name="user_type_id" class="form-control" id="user_type_id" required="required">
                                        <option value="">-Select-</option>
                                        @foreach($usertype as $item)
                                            <option value="{{$item->id}}">{{$item->user_type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            <div class="col">
                                <label for="department" class="control-label">Department</label>
                                <select name="department_id" class="form-control" id="department" required="required">
                                    <option value="">-Select-</option>
                                    @foreach($department as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach  
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-row col-sm-12">
                            <div class="col">
                                <label for="company_id" class="control-label">Organization</label>
                                <select name="company_id" class="form-control" id="company_id" required="required">
                                    <option value="">-Select-</option>
                                    @foreach($company as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <label for="name" class="control-label">Extension Number</label>
                                <input type="text" id="extension_number" name="extension_number" class="form-control"
                                                   placeholder="Extension Number">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info btn-block btn-round" id="activate">Add
                                        User
                        </button>
                    </div>
                </form>
            </div>
        </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
</div>
