<div class="modal-header bg-info">
    <h4 class="title" id="largeModalLabel" style="color: #fff;">Edit Supplier</h4>
    <button type="button" class="close" data-dismiss="modal" data-backdrop="false" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
                        <div class="col-12">
                            <form class="form-group company-form" role="form" method="POST"
                                  action="{{ route('suppliers.update', $get->id) }}"
                                  enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="name" class="control-label">Name*</label>
                                            <input type="text" id="name" name="name" class="form-control"
                                                   value="{{$get->name}}" max="50">
                                        </div>
                                        <div class="col">
                                            <label for="location" class="control-label">Location</label>
                                            <input type="text" id="location" name="location" class="form-control"
                                                   value="{{$get->location}}" max="50">
                                        </div>
                                    </div>

                                    <div class="form-group form-row col-sm-12">
                                        <div class="col">
                                            <label for="email" class="control-label">Email</label>
                                            <input type="email" id="email" name="email" class="form-control"
                                                   value="{{$get->email}}" max="190">
                                        </div>
                                        <div class="col">
                                            <label for="phone" class="control-label">Phone</label>
                                            <input type="tel" id="phone" name="phone" class="form-control"
                                                   value="{{$get->phone}}" max="190">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round btn-block" id="activate">Update
                                        Supplier
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>    
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
</div>